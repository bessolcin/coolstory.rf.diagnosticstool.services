import { saveResponseHttp, saveResponseYCHttp } from './functions/responses/responses';

module.exports = {
	saveResponseYandex: saveResponseYCHttp,
	saveResponseExpress: saveResponseHttp,
};