import { COMMON_HEADERS, DEFAULT_CORS_RESPONSE_YC } from '../../constants';
import { dbRF } from '../../database';
import { PATHS } from '../../paths';

// Сохранять данные о запросе: айпишник, какие то заголовки? айди опроса, генерируемый на клиенте
export async function saveResponseHttp(req: any, res: any) {
	try {
		res.set('Access-Control-Allow-Origin', '*');

		if (req.method === 'OPTIONS') {
			// Send response to OPTIONS requests
			res.set('Access-Control-Allow-Methods', 'GET');
			res.set('Access-Control-Allow-Headers', 'Content-Type');
			res.set('Access-Control-Max-Age', '3600');
			res.status(204).send('');
		} else {

			try {
				const data = req.body;

				// console.log(req.body);
				// console.log(JSON.stringify(req.body));

				const requestData = {
					ip: req.ip,
					ips: req.ips,
					host: req.hostname
				};

				await saveResponse(data.school_id, data.survey_role, data.survey_response, requestData);

				res.json({ result: 'ok' });
			} catch (error) {
				console.error(`Error while saving response. Error: `, JSON.stringify(error), error, 'Req Body:', JSON.stringify(req.body));
				res.json({ result: 'error', error: JSON.stringify(error) });
			}
		}
	} catch (error) {
		res.status(400).send('');
	}
}

export async function saveResponseYCHttp(event: any, context: any) {

	try {
		if (event.httpMethod == 'OPTIONS') {
			return DEFAULT_CORS_RESPONSE_YC;
		}
		else {
			const data = JSON.parse(event.body);

			if (!data.school_id) {
				throw new Error('school_id is empty');
			}

			if (!data.survey_role) {
				throw new Error('survey_role is empty');
			}

			await saveResponse(data.school_id, data.survey_role, data.survey_response);

			return {
				statusCode: 200,
				headers: COMMON_HEADERS,
				body: JSON.stringify({ result: 'ok' })
			}
		}
	} catch (error) {
		console.log(JSON.stringify(event));
		console.error(error);
		return {
			statusCode: 400,
			headers: COMMON_HEADERS,
			body: JSON.stringify({ result: 'error', error: error })
		}
	}
}

export async function saveResponse(communityId: string, role: string, responseData: any, requestData: any = null) {
	console.log(`Saving to path... ${PATHS.all_responses}`);
	const responseItem = {
		...responseData,
		community_id: communityId,
		role: role,
		_timestamp: (new Date()).getTime(),
		_account: 0,
		_request_data: requestData
	}
	const update: any = {};
	const key = dbRF.ref(`${PATHS.all_responses}`).push().key;
	update[`${PATHS.all_responses}/${key}`] = responseItem;
	// update[`${PATHS.community_responses}/${communityId}/${role}`] = responseItem;
	update[`${PATHS.responses_timeline}/${communityId}/${key}`] = responseItem._timestamp;
	console.log(`Response ${communityId} role: ${role} id: ${key}`);

	await dbRF.ref().update(update);
}