import { RolesT, ICommunityOverallStats, IRoleStats, IStatsTable } from '../../../models';
import { db } from '../../database';
import { PATHS } from '../../paths';

const ROLES = ['student', 'teacher', 'parent', 'graduate', 'partner'];

//D3T1L2S4

export async function accountingHandler(req: any, res: any) {
	try {

		await accountStats();

		return res.json({ result: 'ok' });
	} catch (error) {
		console.error('Error while accounting', error);
		return res.status(400).json({ result: 'error', error: error });
	}
}

/** Посчитать статистику для всех */
export async function accountStats() {
	const update: any = {};

	const newResponses: Record<string, string[]> = (await db.ref(PATHS.all_responses).orderByChild('_account').equalTo(0).once('value')).val();
	console.log('New responses count:', Object.keys(newResponses || {}).length);

	await calculateStatsForResponses(newResponses, update, false, false);

	await db.ref().update(update);
}

/** Пересчитать статистику для одного сообщества */
export async function recalculateStatsForCommunity(communityId: string) {
	const update: any = {};
	const newResponses = (await db.ref(PATHS.all_responses).orderByChild('community_id').equalTo(communityId).once('value')).val();

	await calculateStatsForResponses(newResponses, update, false, true);
	await db.ref().update(update);
}

async function calculateStatsForResponses(newResponses: Record<string, string[]>, update: any, isLight: boolean, isRecalculate: boolean = false) {
	const communityStatsIndex: { [comId: string]: CommunityStats } = {};

	getNewResponsesStats(newResponses, communityStatsIndex, update, isLight);

	// const currentCommunitiesStats = await Promise.all(Object.keys(communityStatsIndex)
	// 	.map((id: string) => db.ref(`${PATHS.community_stats}/${id}`)
	// 		.once('value')
	// 		.then(d => d.val() || new CommunityStats())
	// 		.then((v) => { return { id: id, stats: v }; })
	// 	)
	// );
	const currentCommunitiesSurveys = await Promise.all(Object.keys(communityStatsIndex)
		.map((comId: string) => db.ref(`${PATHS.communities}/${comId}/surveys`)
			.once('value')
			.then(d => d.val())
			.then((v) => {

				const surveyIds = Object.keys(v);
				return {
					comId: comId,
					surveyId: surveyIds[surveyIds.length - 1]
				};
			})
		)
	);

	const currentCommunitiesStats = await Promise.all(currentCommunitiesSurveys
		.map(communitySurvey => db.ref(`${PATHS.community_stats}/${communitySurvey.comId}/${communitySurvey.surveyId}`)
			.once('value')
			.then(d => d.val())
			.then((stats) => ({
				...communitySurvey,
				stats: isRecalculate ? new CommunityStats() : stats
			})
			)
		)
	);

	const maximums: CommunityStats = (await db.ref(`${PATHS.max_values}`).once('value')).val();

	for (const comId in communityStatsIndex) {
		if (Object.prototype.hasOwnProperty.call(communityStatsIndex, comId)) {
			const newComStats = communityStatsIndex[comId];
			const currentCommunityInfo = currentCommunitiesStats.filter((el: any) => el.comId == comId)[0];
			let currentComStats: CommunityStats = currentCommunityInfo?.stats;

			currentComStats = calculateStatsForCommunity(currentComStats, newComStats, maximums);
			update[`${PATHS.community_stats}/${comId}/${currentCommunityInfo.surveyId}`] = currentComStats;
		}
	}
}

function calculateStatsForCommunity(currentComStats: CommunityStats, newComStats: CommunityStats, maximums: CommunityStats) {

	const currentComMaximums = new CommunityStats();

	currentComStats.directions = CommunityStats.getDirections();
	currentComStats.tasks = CommunityStats.getTasks();

	for (let i = 0; i < ROLES.length; i++) {
		const role = ROLES[i] as RolesT;
		const newComStatsRole = newComStats.by_roles[role];
		const currentComStatsRole = currentComStats.by_roles[role];

		// if (newComStatsRole.count == 0) {
		// 	continue;
		// }

		currentComStatsRole.count += newComStatsRole.count; // общее количество ответов для роли
		currentComStats.count += newComStatsRole.count;

		for (let d = 1; d <= 9; d++) {
			const D = 'D' + d;

			// if (maxPointsForDirectionByRolePerResponse == 0 || currentComStatsRole.count == 0) {
			// 	continue;
			// }

			const maxPointsForDirectionByRolePerResponse = maximums.by_roles[role].directions[D].sum;
			const directionByRoleMaximum = currentComStatsRole.count * maxPointsForDirectionByRolePerResponse; // возможный максимум баллов для общего количества ответов по направлению для роли

			// плюсуем к сумме по направлению по всему сообществу
			currentComMaximums.directions[D].sum += directionByRoleMaximum // складываем каждый максимум по роли для общего подсчета 
			currentComStatsRole.directions[D].sum += newComStatsRole.directions[D].sum; // общая сумма по направлению для роли
			currentComStats.directions[D].sum += currentComStatsRole.directions[D].sum; // прибавляем сумму для роли в общую сумму по всем ролям

			// todo if currentComStatsRole.count == 0 то не считаем процент
			if (directionByRoleMaximum) {
				const directionIndexInPercent = (currentComStatsRole.directions[D].sum / directionByRoleMaximum) * 100;
				currentComStatsRole.directions[D].index = roundTo2Decimals(directionIndexInPercent);
			}

			currentComStatsRole.directions[D].L1 = Math.max(currentComStatsRole.directions[D].L1, newComStatsRole.directions[D].L1);
			currentComStatsRole.directions[D].L2 = Math.max(currentComStatsRole.directions[D].L2, newComStatsRole.directions[D].L2);
			currentComStatsRole.directions[D].L3 = Math.max(currentComStatsRole.directions[D].L3, newComStatsRole.directions[D].L3);

			for (let j = 1; j <= 3; j++) {
				const T = D + "T" + j;
				const maxPointsForTaskByRolePerResponse = maximums.by_roles[role].tasks[T].sum;

				// if (maxPointsForTaskByRolePerResponse == 0 || currentComStatsRole.count == 0) {
				// 	continue;
				// }

				const taskByRoleMaximum = currentComStatsRole.count * maxPointsForTaskByRolePerResponse;


				currentComMaximums.tasks[T].sum += taskByRoleMaximum;
				currentComStatsRole.tasks[T].sum += newComStatsRole.tasks[T].sum;
				// плюсуем к сумме по задание по всему сообществу
				currentComStats.tasks[T].sum += currentComStatsRole.tasks[T].sum;

				currentComStatsRole.tasks[T].L1 = Math.max(currentComStatsRole.tasks[T].L1, newComStatsRole.tasks[T].L1);
				currentComStatsRole.tasks[T].L2 = Math.max(currentComStatsRole.tasks[T].L2, newComStatsRole.tasks[T].L2);
				currentComStatsRole.tasks[T].L3 = Math.max(currentComStatsRole.tasks[T].L3, newComStatsRole.tasks[T].L3);

				if (taskByRoleMaximum) {
					// TODO если максимум = 0, то не учитываем
					const taskIndexInPercent = (currentComStatsRole.tasks[T].sum / taskByRoleMaximum) * 100;
					currentComStatsRole.tasks[T].index = roundTo2Decimals(taskIndexInPercent); // round to 2 decimals
				}
			}
		}

	}

	// новый цикл чтобы посчитать показатели по сообществу

	for (let i = 1; i <= 9; i++) {
		const D = `D${i}`;

		if (currentComMaximums.directions[D].sum == 0) {
			continue;
		}

		currentComStats.directions[D].index = roundTo2Decimals((currentComStats.directions[D].sum / currentComMaximums.directions[D].sum) * 100);

		for (let j = 1; j <= 3; j++) {
			const T = `${D}T${j}`;
			if (currentComMaximums.tasks[T].sum == 0) {
				continue;
			}
			currentComStats.tasks[T].index = roundTo2Decimals((currentComStats.tasks[T].sum / currentComMaximums.tasks[T].sum) * 100);
		}
	}

	return currentComStats;
	// console.log(currentComStats);
}

function getNewResponsesStats(newResponses: any, communityStatsIndex: { [comId: string]: CommunityStats; }, update: any, isLight: boolean) {
	let communityStats: CommunityStats;
	for (const key in newResponses) {
		if (Object.prototype.hasOwnProperty.call(newResponses, key)) {
			const response = newResponses[key];

			const isLightResponse = !!response.light;
			if (isLightResponse !== isLight) {
				continue;
			}

			if (!response.community_id || response.community_id == 'null') {
				continue;
			}

			if (!communityStatsIndex[response.community_id]) {
				communityStatsIndex[response.community_id] = new CommunityStats();
			}

			communityStats = communityStatsIndex[response.community_id];

			communityStats.by_roles[<RolesT>response.role].count += 1;
			communityStats.count += 1;

			/*
			{
				...
				question6: ["D6T2L2S1", "D6T1L1S1"]	
				...
			}
			*/
			for (const qcode in response) {
				if (Object.prototype.hasOwnProperty.call(response, qcode)) {
					const qvalues = response[qcode];

					if (Array.isArray(qvalues)) {
						for (let i = 0; i < qvalues.length; i++) {
							const value = qvalues[i];
							if (value == 'none') {
								break;
							}
							const parsedValue = parseQuestionValue(value);

							// communityStats.directions[parsedValue.direction].sum += parsedValue.level;
							// communityStats.tasks[parsedValue.task].sum += parsedValue.level;
							try {
								communityStats.by_roles[<RolesT>response.role].directions[parsedValue.direction].sum += parsedValue.level;
								(<any>communityStats.by_roles[<RolesT>response.role].directions[parsedValue.direction])[parsedValue.level_code] += 1;

								communityStats.by_roles[<RolesT>response.role].tasks[parsedValue.task].sum += parsedValue.level;
								(<any>communityStats.by_roles[<RolesT>response.role].tasks[parsedValue.task])[parsedValue.level_code] += 1;
							} catch (error) {
								console.error(error);
							}
						}
					}
				}
			}

			update[`${PATHS.all_responses}/${key}/_account`] = 1;
		}
	}
}

function roundTo2Decimals(num: number) {
	return Math.round(num * 100) / 100;;
}

function parseQuestionValue(value: string) {
	return {
		direction: value.substr(0, 2),
		task: value.substr(0, 4),
		level: parseInt(value[5]),
		level_code: value.substr(4, 2)
	}
}

export class CommunityStats implements ICommunityOverallStats {
	public count: number = 0;
	public directions: IStatsTable = CommunityStats.getDirections();
	public tasks: IStatsTable = CommunityStats.getTasks();

	public by_roles: IRoleStats = {
		student: {
			count: 0,
			directions: CommunityStats.getDirections(),
			tasks: CommunityStats.getTasks(),
		},
		teacher: {
			count: 0,
			directions: CommunityStats.getDirections(),
			tasks: CommunityStats.getTasks(),
		},
		parent: {
			count: 0,
			directions: CommunityStats.getDirections(),
			tasks: CommunityStats.getTasks(),
		},
		graduate: {
			count: 0,
			directions: CommunityStats.getDirections(),
			tasks: CommunityStats.getTasks(),
		},
		partner: {
			count: 0,
			directions: CommunityStats.getDirections(),
			tasks: CommunityStats.getTasks(),
		}
	};

	static getDirections() {
		return {
			"D1": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D2": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D3": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D4": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D5": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D6": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D7": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D8": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D9": { sum: 0, L1: 0, L2: 0, L3: 0 }
		};
	}

	static getTasks() {
		return {
			"D1T1": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D1T2": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D1T3": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D2T1": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D2T2": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D2T3": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D3T1": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D3T2": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D3T3": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D4T1": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D4T2": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D4T3": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D5T1": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D5T2": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D5T3": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D6T1": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D6T2": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D6T3": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D7T1": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D7T2": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D7T3": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D8T1": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D8T2": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D8T3": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D9T1": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D9T2": { sum: 0, L1: 0, L2: 0, L3: 0 },
			"D9T3": { sum: 0, L1: 0, L2: 0, L3: 0 }
		}
	}

}