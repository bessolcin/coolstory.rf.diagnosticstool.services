import got from 'got';
import moment = require('moment');

import { COMMON_HEADERS, DEFAULT_CORS_RESPONSE_YC, IYandexEvent } from '../../constants';
import { db } from '../../database';
import { PATHS } from '../../paths';
import { CommunityStats } from '../accounting';
import { ICommunityDB, ICommunityFullModel, ICommunityViewModel, ISurvey } from '../../../models';

const STAT_SAMPLE_COUNT = 384.16; // рассчитанная выборка без данных об общей совокупности
const STAT_SAMPLE_COUNT_M1 = 383.16;
const EMAIL_API_URL = 'https://events.sendpulse.com/events/id/7647b39aed964cce81c9f615f2f50f6c/7374453';


/** @deprecated Use Api */
// export async function communitiesHttp(req: any, res: any) {
// 	res.set('Access-Control-Allow-Origin', '*');

// 	if (req.method === 'OPTIONS') {
// 		// Send response to OPTIONS requests
// 		res.set('Access-Control-Allow-Methods', 'GET,POST');
// 		res.set('Access-Control-Allow-Headers', 'Content-Type');
// 		res.set('Access-Control-Max-Age', '3600');
// 		return res.status(204).send('');
// 	} else {

// 		try {
// 			if (req.method == 'POST') {

// 				const community = await createCommunity({
// 					name: req.body.name || null,
// 					email: req.body.email,

// 					// organization: {
// 						country: req.body.country,
// 						region: req.body.region,
// 						city: req.body.city,
// 						school: req.body.school,
// 					// },

// 					organization: {
// 						data_type: 'manual',
// 						country: req.body.country,
// 						region: req.body.region,
// 						city: req.body.city,
// 						name: req.body.school,
// 					},

// 					teachers_count: req.body.teachers_count,
// 					students_count: req.body.students_count,
// 					graduate_estimate: req.body.graduate_estimate
// 				}, req.query.schoolId || null);

// 				if (community.email) {
// 					try {
// 						await got.post(EMAIL_API_URL, {
// 							json: {
// 								email: community.email,
// 								phone: "+123456789",
// 								school_id: community.id,
// 								user_name: community.name,
// 								school_name: community.school,
// 								students_count: community.students_count,
// 								teachers_count: community.teachers_count,
// 								graduate_count: community.graduate_estimate,
// 								community_size: community.approx_community_size,
// 								sample_count: community.sample_count.total
// 							}
// 						});
// 					} catch (error) {
// 						const errorMessage = `Error while sending email to ${community.email}. Error: ${(<any>error).message}`;
// 						console.error(errorMessage, error);
// 					}
// 				}

// 				if (req.query.html == 'true') {
// 					return res.send(`<p class="rf-H3">Вы успешно зарегистрированы!</p><br>
// 					<p class="rf-subheader">Скопируйте и сохраните вашу ссылку на опрос сообщества:</p><br>
// 					<a href="https://concept.rybakovfoundation.ru/diag/${community.id}" target="_blank" class="rf-link">https://concept.rybakovfoundation.ru/diag/${community.id}</a>
// 					<br>
// 					<br>
// 					<p class="rf-subheader">И ссылку на результаты опроса:</p><br>
// 					<a href="https://concept.rybakovfoundation.ru/diag/stats?c=${community.id}" target="_blank" class="rf-link">https://concept.rybakovfoundation.ru/diag/stats?c=${community.id}</a>
// 					<br>`);
// 				}
// 				return res.json({ message: `Ваша ссылка на опрос сообщества: https://concept.rybakovfoundation.ru/diag/${community.id}`, result: 'ok', id: community.id });
// 			}

// 			if (req.method == 'GET') {
// 				const community = await getCommunityById(req.query.id);
// 				return res.json({ result: community });
// 			}

// 		} catch (error) {
// 			console.error(`Error while creating community: ${req.body}`);
// 			return res.json({ result: 'error', id: (<any>error).message });
// 		}
// 	}
// 	return res.status(403).send('');
// }

// /**
//  * HTTP Функция для Яндекс.Облака
//  * @param event 
//  * @deprecated Use Api 
//  */
// export async function getCommunityByIdYC(event: IYandexEvent) {
// 	try {

// 		if (event.httpMethod == 'OPTIONS') {
// 			return DEFAULT_CORS_RESPONSE_YC;
// 		}
// 		else {

// 			const communnity = await getCommunityById(event.queryStringParameters['id']);

// 			return {
// 				statusCode: 200,
// 				headers: COMMON_HEADERS,
// 				body: JSON.stringify({ result: communnity })
// 			}
// 		}
// 	} catch (error) {
// 		return {
// 			statusCode: 400,
// 			headers: COMMON_HEADERS,
// 			body: JSON.stringify({ result: 'error', error: JSON.stringify(error) })
// 		}
// 	}
// }

/**
 * HTTP Функция для Яндекс.Облака
 * @deprecated Use Api 
 * @param event 
 */
// export async function createCommunityYC(event: IYandexEvent) {
// 	try {

// 		if (event.httpMethod == 'OPTIONS') {
// 			return DEFAULT_CORS_RESPONSE_YC;
// 		}
// 		else if (event.httpMethod == 'POST') {

// 			const communnity = await createCommunity(JSON.parse(event.body), event.queryStringParameters['schoolId']);

// 			return {
// 				statusCode: 200,
// 				headers: COMMON_HEADERS,
// 				body: JSON.stringify({ result: communnity })
// 			}
// 		} else {
// 			return {
// 				statusCode: 400,
// 				headers: COMMON_HEADERS,
// 				body: JSON.stringify({ result: 'error', error: 'Cannot get' })
// 			}
// 		}
// 	} catch (error) {
// 		return {
// 			statusCode: 400,
// 			headers: COMMON_HEADERS,
// 			body: JSON.stringify({ result: 'error', error: JSON.stringify(error) })
// 		}
// 	}
// }

/**
 * @deprecated Использовать репозиторий
 * @param comId 
 * @returns 
 */
export async function getCommunityById(comId: string) {
	const community: ICommunityDB = (await db.ref(`${PATHS.communities}/${comId}`).once('value')).val();

	if (!community) {
		return null;
	}

	let surveyStats = null;
	if (community.surveys) {
		surveyStats = await Promise.all(Object.keys(community.surveys)
			.map(surveyId => db.ref(`${PATHS.community_stats}/${comId}/${surveyId}`).once('value').then(d => {
				const survey = community.surveys[surveyId];
				survey.id = surveyId;
				survey.stats = d.val();
				survey.start_date_str = survey.start_date_str || moment(survey.start_date).format('DD.MM.YYYY');
				return survey;
			}))
		);
		surveyStats.sort((a, b) => a.start_date - b.start_date);
	}

	return <ICommunityFullModel>{
		id: comId,
		...community,
		stats: surveyStats ? surveyStats[surveyStats.length - 1].stats : null,
		surveys: <any><unknown>surveyStats
	};
}

/**
 * Создать сообщество в базе Инструмента диагностики
 * @param community модель сообщества с клиента
 * @param existingCommunityId опционально ИД сообщества если уже сгенерирован
 * @returns Созданное сообщество
 * @deprecated - Use manager
 */
export async function createCommunity(community: ICommunityViewModel, existingCommunityId?: string) {
	const communityDb: ICommunityDB = <any>community;
	communityDb._timestamp = (new Date()).getTime();

	communityDb.teachers_count = parseInt(<any>communityDb.teachers_count);
	communityDb.students_count = parseInt(<any>communityDb.students_count);
	community.graduate_estimate = parseInt(<any>community.graduate_estimate);

	const parents = communityDb.students_count;
	const graduate = Math.round(community.graduate_estimate * 0.1);

	communityDb.approx_community_size = communityDb.students_count + communityDb.teachers_count + parents + graduate;

	communityDb.sample_count = {
		student: correctSampleSize(communityDb.students_count),
		teacher: correctSampleSize(communityDb.teachers_count),
		parent: correctSampleSize(parents),
		graduate: correctSampleSize(graduate),
		total: 0
	};
	communityDb.sample_count.total = Math.round(communityDb.sample_count.student +
		communityDb.sample_count.teacher +
		communityDb.sample_count.parent +
		communityDb.sample_count.graduate);

	let newCommunityKey;
	const communityExists = (await db.ref(`${PATHS.communities}/${existingCommunityId}`).once('value')).exists();
	if (!existingCommunityId || communityExists) {
		// Если не передали или такое сообщество уже зарегано, то генерируем новый
		newCommunityKey = await getCommunityId(); // db.ref(`${PATHS.communities}`).push().key;
	}
	else {
		newCommunityKey = existingCommunityId;
		communityDb._is_rsa = true;
	}

	const update: any = {};
	const comStats = new CommunityStats();
	const surveyKey = <string>db.ref(`${PATHS.communities}/${newCommunityKey}/surveys`).push().key;

	const now = new Date();
	const communitySurvey: ISurvey = {
		id: surveyKey,
		start_date: now.getTime(),
		start_date_str: moment().format('DD.MM.YYYY'),
		datetime_offset: now.getTimezoneOffset()
	};

	communityDb.surveys = {};
	communityDb.surveys[surveyKey] = communitySurvey;
	update[`${PATHS.communities}/${newCommunityKey}`] = communityDb;
	update[`${PATHS.community_stats}/${newCommunityKey}/${surveyKey}`] = comStats;


	await db.ref().update(update);

	return {
		id: newCommunityKey,
		...communityDb,
		stats: comStats
	};
}

/**
 * @deprecated Используй метод из менеджера
 * @param cid 
 */
export async function createCommunitySurvey(cid: string) {
	// const communityDb = (await db.ref(`${PATHS.communities}/${cid}`).once('value')).val();

	const now = new Date();
	const communitySurvey: ISurvey = {
		start_date: now.getTime(),
		datetime_offset: now.getTimezoneOffset()
	};

	const surveyKey = <string>db.ref(`${PATHS.communities}/${cid}/surveys`).push().key;
	const update: any = {};
	update[`${PATHS.community_stats}/${cid}/${surveyKey}`] = new CommunityStats();
	update[`${PATHS.communities}/${cid}/surveys/${surveyKey}`] = communitySurvey;

	db.ref().update(update);
}

/**
 * 
 * @deprecated Используй метод из менеджера
 * @param cid 
 * @param name 
 * @param email 
 */
export async function changeCommunityOwner(cid: string, name: string, email: string) {
	const update: any = {};
	update[`${PATHS.communities}/${cid}/name`] = name;
	update[`${PATHS.communities}/${cid}/email`] = email;
	db.ref().update(update);
}

export async function getCommunityId() {
	let newComId = <string>db.ref(PATHS.communities).push().key;
	let comCode = shortenCode(newComId, 6);

	let counter = 0;
	while ((await db.ref(`${PATHS.communities}/${comCode}`).once('value')).exists()) {
		newComId = <string>db.ref(PATHS.communities).push().key;
		comCode = shortenCode(newComId, 6);

		counter++;
		console.log(`Created teamcode in ${counter} time.`)
		if (counter > 10) {
			throw new Error('Cannot create unique team code 10 times!');
		}
	}

	return comCode;
}

function shortenCode(code: string, length: number) {
	const sanitized = code?.replace(/[_\-]/g, '');
	return sanitized?.substring(sanitized.length - length).toUpperCase();
}

/**
 * формула корректировки когда известна общая совокупность
 * @param totalPopulation Размер всей популяции
 * @returns 
 */
export function correctSampleSize(totalPopulation: number) {
	if (totalPopulation == 0) {
		return 0;
	}
	return Math.round(STAT_SAMPLE_COUNT / (1 + STAT_SAMPLE_COUNT_M1 / totalPopulation));
}