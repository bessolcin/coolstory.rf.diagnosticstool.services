import moment = require('moment');
import { getOrgName, ICommunityDB, ICommunityViewModel, ISurvey } from '../../../../models';
import { ICreateCommunityRequest } from '../../../../models/common';
import { CommunitiesRepository, ResponsesRepository } from '../repositories';
import { CommunityStats } from '../../accounting';
import { EMAIL_TEMPLATES, REGISTRATION_EMAIL_TEMPLATE } from '../constants';
import { mapCommunityToEmailData, sendEmailTemplate } from '../services';
import { getCommunityId, correctSampleSize } from '../../community';

export interface ICommunitySizeParams {
	teachers_count: number;
	students_count: number;
	graduate_estimate: number;
}

export class CommunitiesManager {

	constructor(
		private communities: CommunitiesRepository,
		private responses: ResponsesRepository) {
	}

	public getById(comId: any, withStats?: boolean) {
		return this.communities.getById(comId, withStats);
	}

	public async createCommunity(community: ICreateCommunityRequest, existingCommunityId?: string) {
		const {
			teachers_count,
			students_count,
			graduate_estimate,
			parents_count,
			graduates_count,
			approx_community_size
		} = this.getCommunityMembersCount(community);

		let { newCommunityId, isRsa } = await this.getCommunityId(existingCommunityId);

		const survey = this.createSurvey();
		const comStats = new CommunityStats();

		const communityDb: ICommunityDB = {
			id: newCommunityId,
			...community,
			_timestamp: (new Date()).getTime(),
			_is_rsa: isRsa,

			teachers_count,
			students_count,
			graduate_estimate,
			approx_community_size,

			sample_count: this.getSample({ teachers_count, students_count, parents_count, graduates_count }),

			surveys: {
				[`${survey.id}`]: survey
			}
		};

		await this.communities.create(communityDb, survey, comStats);

		return {
			...communityDb,
			stats: comStats
		};
	}

	public async changeCommunityOwner(comId: string, name: string, email: string) {
		if (!(await this.communities.exists(comId))) {
			throw new Error('Invalid request: community not found');
		}

		await this.communities.update(comId, {
			name,
			email
		});
	}

	public async changeCommunitySize(comId: string, size: ICommunitySizeParams) {
		const count = this.getCommunityMembersCount(size);
		const sample_count = this.getSample(count);
		const newSize = {
			teachers_count: count.teachers_count,
			students_count: count.students_count,
			graduate_estimate: count.graduate_estimate,
			approx_community_size: count.approx_community_size,
			sample_count
		};
		await this.communities.update(comId, newSize);

		return newSize;
	}

	public async createCommunitySurvey(comId: string) {
		const community = await this.getById(comId);
		if (!community) {
			throw new Error('Invalid request: community not found');
		}

		const communitySurvey: ISurvey = this.createSurvey();
		const communityStats = new CommunityStats();
		await this.communities.createSurvey(comId, communitySurvey, communityStats);

		const emailTemplateId = EMAIL_TEMPLATES.SURVEYS;
		if (emailTemplateId) {
			await sendEmailTemplate(emailTemplateId, community.email, {
				user_name: community.name,
				school_id: community.id,
				school_name: getOrgName(community.organization)
			});
		}
	}

	public async getActivity(comId: string) {
		const msInADay = 1000 * 60 * 60 * 24;
		const community = await this.communities.getById(comId);

		if (community && community.surveys) {
			const responsesTask = this.responses.getResponsesTimeline(comId);

			const surveyIds = Object.keys(community.surveys);
			let index = 0;
			let data = this.getNextSurvey(index, surveyIds, community.surveys as any);

			const surveyActivity: any = {};
			let histogramDict: Record<number, number> = {};

			const responsesSnap = await responsesTask;
			const comResponses = responsesSnap.val();
			for (const rid in comResponses) {
				if (Object.prototype.hasOwnProperty.call(comResponses, rid)) {
					const responseTimestamp: number = comResponses[rid];
					if (responseTimestamp > data.survey.start_date) {

						if (data.nextSurvey && responseTimestamp >= data.nextSurvey.start_date) {
							surveyActivity[data.surveyId] = histogramDict;
							histogramDict = {};
							index++;
							data = this.getNextSurvey(index, surveyIds, community.surveys as any);
						}

						const responseDate = Math.trunc(responseTimestamp / msInADay) * msInADay;
						if (typeof (histogramDict[responseDate]) == 'undefined') {
							histogramDict[responseDate] = 0;
						}
						histogramDict[responseDate]++;
					}
				}
			}
			surveyActivity[data.surveyId] = histogramDict;

			return surveyActivity;
		}
		return null;
	}

	private getNextSurvey(index: number, surveyIds: string[], surveys: Record<string, ISurvey>) {
		let surveyId = surveyIds[index];
		let nextSurveyId: string | null = null;

		if (index + 1 < surveyIds.length) {
			nextSurveyId = surveyIds[index + 1];
		}

		let survey = surveys[surveyId as any];
		let nextSurvey = nextSurveyId ? surveys[nextSurveyId as any] : null;

		return {
			surveyId,
			nextSurveyId,
			survey,
			nextSurvey
		}
	}

	private async getCommunityId(existingCommunityId: string | undefined) {
		let newCommunityId;
		let isRsa = false;
		const communityExists = await this.communities.exists(existingCommunityId);

		if (existingCommunityId && !communityExists) {
			newCommunityId = existingCommunityId;
			isRsa = true;
		}
		else {
			newCommunityId = await getCommunityId();
		}
		return { newCommunityId, isRsa };
	}

	private createSurvey() {
		const surveyKey = this.communities.getKey(); // <string>db.ref(`${PATHS.communities}/${newCommunityKey}/surveys`).push().key;
		const now = new Date();

		const survey: ISurvey = {
			id: <string>surveyKey,
			start_date: now.getTime(),
			start_date_str: moment().format('DD.MM.YYYY'),
			datetime_offset: now.getTimezoneOffset()
		};

		return survey;
	}

	private getCommunityMembersCount(
		{ teachers_count, students_count, graduate_estimate }: ICommunitySizeParams) {
		const parents_count = students_count;
		const graduates_count = Math.round(graduate_estimate * 0.1);
		const approx_community_size = students_count + teachers_count + parents_count + graduates_count;

		return {
			teachers_count,
			students_count,
			graduate_estimate,
			parents_count,
			graduates_count,
			approx_community_size
		}
	}

	private getSample({ teachers_count, students_count, parents_count, graduates_count }: Record<string, number>) {
		const sample = {
			teacher: correctSampleSize(teachers_count),
			student: correctSampleSize(students_count),
			parent: correctSampleSize(parents_count),
			graduate: correctSampleSize(graduates_count),
			total: 0
		};
		sample.total = Math.round(sample.student + sample.teacher + sample.parent + sample.graduate);
		return sample;
	}

}