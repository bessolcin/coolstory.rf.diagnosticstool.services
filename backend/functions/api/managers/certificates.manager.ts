import { getOrgName, ICommunity, isDadataOrg, ISurvey } from '../../../../models';
import { CommunitiesRepository } from '../repositories';
import { HtmlToImageService } from '../services/pdf';

const D = ['D7', 'D1', 'D2', 'D3', 'D4', 'D5', 'D6', 'D8', 'D9'];
const months = ['январе', 'феврале', 'марте', 'апреле', 'мае', 'июне', 'июле', 'августе', 'сентябре', 'октябре', 'ноябре', 'декабре'];

export class CertificatesManager {

	constructor(
		private communities: CommunitiesRepository,
		private htmlToImageService: HtmlToImageService) {

	}

	public async createCertificate(comId: string, surveyId: string) {
		const community = await this.communities.getById(comId, true);

		if (community) {
			const survey = community.surveys?.filter((s: ISurvey) => s.id == surveyId)[0];
			if (survey) {
				const surveyDate = (new Date(survey.start_date));
				const values = D.reduce((prev: any, d: string) => {
					prev[d] = Math.round(survey?.stats?.directions[d]?.index || 0) + '%';
					return prev;
				}, {});

				const org = community.organization

				const buffer = await this.htmlToImageService.generatePDFFromTemplate('certificate.html', {
					school: isDadataOrg(org) ? org.name.short_with_opf : org?.name,
					month: months[surveyDate.getMonth()],
					year: surveyDate.getFullYear(),
					...values
				});

				const filename = encodeURIComponent(this.getFileName(community, surveyDate));

				return {
					filename,
					buffer
				}
			}
		}
		return null;
	}

	private getFileName(community: ICommunity, surveyDate: Date): string {
		const orgName = getOrgName(community.organization);
		return `${orgName} - Сертификат о прохождении инструмента диагностики в ${months[surveyDate.getMonth()]} ${surveyDate.getFullYear()} г`;
	}
}