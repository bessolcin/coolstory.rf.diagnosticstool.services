import { database as db } from 'firebase-admin';
import moment = require('moment');
import { PATHS } from '../../../paths';
import { CommunitiesManager } from './communities.manager';

export class RequestsManager {

	private requestsRef: db.Reference;

	constructor(
		private db: db.Database,
		private communityManager: CommunitiesManager
	) {
		this.requestsRef = db.ref(PATHS.requests);
	}

	public async createSurveyRequest(comId: string) {
		return this.createRequest(RequestType.SURVEYS, { com_id: comId });
	}

	public async createOwnerChangeRequest(comId: string, oldEmail: string, name: string, email: string) {
		return this.createRequest(RequestType.OWNER_CHANGE, {
			com_id: comId,
			old_email: oldEmail,
			requester_name: name,
			requester_email: email
		});
	}

	private async createRequest(requestType: RequestType, data: { com_id: string } & any) {
		const now = moment();
		const reqId = this.requestsRef.push().key as string;
		const request: IDTRequest = {
			id: reqId,
			type: requestType,
			created: now.valueOf(),
			created_str: now.toJSON(),
			status: RequestStatus.NEW,
			data: data
		};

		await this.requestsRef.child(reqId).set(request);
		return request;
	}


	public async approveRequest(reqId: string) {
		const request = await this.getRequest(reqId);

		if (request.status == RequestStatus.APPROVED) {
			return request;
		}
		const approveTask = this.setRequestStatus(request)
		switch (request.type) {
			case RequestType.SURVEYS:
				await this.communityManager.createCommunitySurvey(request.data.com_id);
				break;
			case RequestType.OWNER_CHANGE:
				await this.communityManager.changeCommunityOwner(request.data.com_id, request.data.requester_name, request.data.requester_email);
				break;
			default:
				break;
		}
		await approveTask;

		return request;
	}

	private async setRequestStatus(request: IDTRequest) {
		request.status = RequestStatus.APPROVED;
		const statusRef = this.requestsRef.child(`${request.id}/status`);
		await statusRef.set(RequestStatus.APPROVED);
	}

	public async getRequest(reqId: string): Promise<IDTRequest> {
		return (await this.requestsRef.child(reqId).once('value')).val();
	}


}

export interface IDTRequest {
	id: string;
	type: RequestType;
	created: number;
	created_str: string;
	status: RequestStatus;
	data: any;
}

export enum RequestStatus {
	NEW = 'NEW',
	APPROVED = 'APPROVED'
}

export enum RequestType {
	SURVEYS = 'SURVEYS',
	OWNER_CHANGE = 'OWNER_CHANGE'
}