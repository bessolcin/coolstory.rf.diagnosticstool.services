import { database } from 'firebase-admin';
import moment = require('moment');
import { IPayment, PyamentStatus } from '../../../../models';
import { PATHS } from '../../../paths';

export class PaymentsManager {


	private paymentsRef: database.Reference;

	constructor(private db: database.Database) {
		this.paymentsRef = db.ref(PATHS.payments);
	}

	public async createPayment(comId: string) {
		const payment = this.getNewPayment(comId);
		await this.paymentsRef.child(payment.id).set(payment);

		return payment;
	}

	public async confirmPayment(paymentData: any & { InvId: string, Shp_payment: string, source: 'callback' | 'api' }) {
		const now = moment();
		const paymentRef = this.paymentsRef.child(paymentData.Shp_payment);
		await paymentRef.update({
			updated: now.valueOf(),
			updated_str: now.toJSON(),
			status: PyamentStatus.PAID,
			payment_data: paymentData
		});
	}

	public async bindWithCommunity(payment_id: string, comId: string) {
		const now = moment();
		const paymentRef = this.paymentsRef.child(payment_id);

		await paymentRef.update({
			community_id: comId,
			updated: now.valueOf(),
			updated_str: now.toJSON(),
			status: PyamentStatus.PAID,
		});
	}

	private getNewPayment(comId: string) {
		const now = moment();

		return <IPayment>{
			id: now.valueOf().toString(),
			community_id: comId,
			created: now.valueOf(),
			created_str: now.toJSON(),
			status: PyamentStatus.NEW
		}
	}
}