import * as crypto from 'crypto';
const payment = 'https://auth.robokassa.ru/Merchant/Index.aspx?MerchantLogin=coolstorypro&InvId=0&Culture=ru&Encoding=utf-8&Description=Доступ к сервису Инструмента Диагностики&OutSum=300.00&Receipt={"items":[{"name":"Доступ к сервису Инструмента Диагностики","quantity":1,"sum":300,"payment_method":"full_payment","payment_object":"payment","tax":"none"}]}&shp_interface=link&SignatureValue=ab5cbf0053f3e8a3c9cde1983d209a6c'

// const password1 = 'smlCn45MB8qurK0sp3ty';
// const password2 = 'Vbdjye08f05WYgWsL9iB';
const password1 = process.env.ROBOKASSA_P1 || 'l8rNy7PQ8iCQ7N0UGEjJ';
const password2 = process.env.ROBOKASSA_P2 || 's4eVbeczMbe5j4f5M2TY';
const merchantLogin = process.env.ROBOKASSA_LOGIN || 'coolstorypro';
const isTest = process.env.ROBOKASSA_PROD !== 'true';

export function getSignedPaymentUrl(paymentId: string, comId: string) {
	const paymentParams = getPaymenParamsMap(paymentId, comId);
	const signatureBase = getSignatureBase(paymentParams, password1);
	const signature = getSignature(signatureBase);
	paymentParams.SignatureValue = signature;

	const str = Object.entries(paymentParams).map(([key, val]: [string, string]) => key + '=' + val).join('&')

	return `https://auth.robokassa.ru/Merchant/Index.aspx?${str}`;
}

export function checkCallbackParameters(rbPayment: IRobokassaParams) {
	const signatureBase = getSignatureBaseCallback(rbPayment, password2)
	const signature = getSignature(signatureBase);

	return rbPayment.SignatureValue == signature.toUpperCase();
}

function getSignature(data: string) {
	// SignatureValue: 'ab5cbf0053f3e8a3c9cde1983d209a6c',
	return crypto.createHash('md5').update(data).digest("hex");
}

function getSignatureBase(rbPayment: IRobokassaParams, pasword: string) {
	const customParams = Object.keys(rbPayment).filter(p => p.toLowerCase().startsWith('shp')).sort();
	const customValues = customParams.map(p => `${p}=${(<any>rbPayment)[p]}`);
	return [merchantLogin, rbPayment.OutSum, rbPayment.InvId, rbPayment.Receipt, pasword, ...customValues].join(':')
}
function getSignatureBaseCallback(rbPayment: IRobokassaParams, pasword: string) {
	const customParams = Object.keys(rbPayment).filter(p => p.toLowerCase().startsWith('shp')).sort();
	const customValues = customParams.map(p => `${p}=${(<any>rbPayment)[p]}`);
	return [rbPayment.OutSum, rbPayment.InvId, pasword, ...customValues].join(':')
}

function getPaymenParamsMap(paymentId: string, comId: string) {
	// return {
	// 	MerchantLogin: 'demo',
	// 	OutSum: '11.00',
	// 	Description: 'Покупка в демо магазине'
	// }

	const params = {
		Culture: 'ru',
		Description: 'Доступ к сервису Инструмента Диагностики',
		Encoding: 'utf-8',
		InvId: '0',
		// IsTest: 1,
		MerchantLogin: merchantLogin,
		OutSum: '300.00',
		Receipt: JSON.stringify(getReceiptObject()),
		Shp_comid: comId,
		Shp_interface: 'link',
		Shp_payment: paymentId
	} as IRobokassaParams;

	if (isTest) {
		params.IsTest = 1;
	}

	return params;
}

function getReceiptObject() {
	return {
		items: [{
			"name": "Доступ к сервису Инструмента Диагностики",
			"quantity": 1,
			"sum": 300,
			"payment_method": "full_payment",
			"payment_object": "payment",
			"tax": "none"
		}]
	}
}

export interface IRobokassaParams {
	Culture: string;
	Description: string;
	Encoding: string;
	InvId: string;
	IsTest?: number;
	MerchantLogin: string;
	OutSum: string;
	Receipt: string;
	Shp_interface: string;
	Shp_comid: string;
	Shp_payment: string;
	SignatureValue?: string;
}