import got from 'got';
import { getOrgName, ICommunityDB } from '../../../../models';

const EMAIL_API_URL = 'https://events.sendpulse.com';

export async function sendEmailTemplate(eventId: string, email: string, templateData: any) {
	return await got.post(`${EMAIL_API_URL}/events/id/${eventId}/7374453`, {
		json: {
			email: email,
			phone: "+123456789",
			...templateData
		}
	});
}

export function mapCommunityToEmailData(community: ICommunityDB) {
	return {
		school_id: community.id,
		user_name: community.name,
		school_name: getOrgName(community.organization),
		students_count: community.students_count,
		teachers_count: community.teachers_count,
		graduate_count: community.graduate_estimate,
		community_size: community.approx_community_size,
		sample_count: community.sample_count.total
	};
}