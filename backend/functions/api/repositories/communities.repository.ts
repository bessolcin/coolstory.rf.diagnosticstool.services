import { database } from 'firebase-admin';
import moment = require('moment');
import { ICommunityDB, ICommunityFullModel, ISurvey } from '../../../../models';
import { PATHS } from '../../../paths';
import { CommunityStats } from '../../accounting';

export class CommunitiesRepository {


	private communitiesRef: database.Reference;

	constructor(private db: database.Database) {
		this.communitiesRef = db.ref(PATHS.communities);
	}

	public async getById(comId: string, withStats?: boolean) {
		const community: ICommunityDB = (await this.communitiesRef.child(comId).once('value')).val();

		if (!community) {
			return null;
		}

		let surveyStats: ISurvey[] | null = null;
		if (community.surveys && withStats === true) {
			surveyStats = await Promise.all(Object.keys(community.surveys)
				.map(surveyId => this.db.ref(`${PATHS.community_stats}/${comId}/${surveyId}`).once('value').then(d => {

					const survey = community.surveys[surveyId];
					survey.id = surveyId;
					survey.stats = d.val();
					survey.start_date_str = survey.start_date_str || moment(survey.start_date).format('DD.MM.YYYY');
					return survey;
				}))
			);
			surveyStats.sort((a, b) => a.start_date - b.start_date);
		}

		return <ICommunityFullModel>{
			id: comId,
			...community,
			stats: surveyStats ? surveyStats[surveyStats.length - 1].stats : null,
			surveys: <any><unknown>surveyStats || community.surveys
		};
	}

	public async create(community: ICommunityDB, survey: ISurvey, stats: CommunityStats) {
		const update: any = {};
		update[`${PATHS.communities}/${community.id}`] = community;
		update[`${PATHS.community_stats}/${community.id}/${survey.id}`] = stats;

		await this.db.ref().update(update);
	}

	public update(comId: string, update: Partial<ICommunityDB>) {
		return this.communitiesRef.child(comId).update(update);
	}

	public createSurvey(comId: string, survey: ISurvey, communityStats: CommunityStats) {
		const update: any = {};
		
		update[`${PATHS.communities}/${comId}/surveys/${survey.id}`] = survey;
		update[`${PATHS.community_stats}/${comId}/${survey.id}`] = communityStats;

		return this.db.ref().update(update);
	}

	public async exists(existingCommunityId: string | undefined) {
		return (await this.db.ref(`${PATHS.communities}/${existingCommunityId}`).once('value')).exists();
	}

	public getKey() {
		return this.db.ref().push().key;
	}
}