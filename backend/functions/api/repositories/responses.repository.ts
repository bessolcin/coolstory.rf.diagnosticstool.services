import { database } from 'firebase-admin';
import { PATHS } from '../../../paths';

export class ResponsesRepository {

	private allResponsesRef: database.Reference;
	private responsesTimelineRef: database.Reference;

	constructor(db: database.Database) {
		this.allResponsesRef = db.ref(PATHS.all_responses);
		this.responsesTimelineRef = db.ref(PATHS.responses_timeline);
	}

	public async getResponses(comId: string) {
		return this.allResponsesRef.orderByChild('community_id').equalTo(comId).once('value')
	}

	public getResponsesTimeline(comId: string) {
		return this.responsesTimelineRef.child(comId).once('value');
	}
}