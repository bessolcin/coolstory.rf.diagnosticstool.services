// require('dotenv').config();
import * as dotenv from 'dotenv'
dotenv.config({ path: 'backend/.env' });
import * as express from 'express';
import * as cors from 'cors'
// import { readFile } from 'fs/promises'

import { db } from '../../database';

import { PATHS } from '../../paths';
import { apiRouter } from './routes/api';
import { ICommunityDB } from '../../../models';

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(apiRouter);


// /**
//  * Создать сообщество (старый метод для совместимости)
//  */
// app.post('/', async (req: Request, res: Response) => {

// 	const community = await createCommunity({
// 		name: req.body.name,
// 		email: req.body.email,

// 		country: req.body.country,
// 		region: req.body.region,
// 		city: req.body.city,
// 		school: req.body.school,

// 		organization: {
// 			data_type: 'manual',
// 			country: req.body.country,
// 			region: req.body.region,
// 			city: req.body.city || null,
// 			settlement: req.body.settlement || null,
// 			name: req.body.school
// 		},

// 		teachers_count: req.body.teachers_count,
// 		students_count: req.body.students_count,
// 		graduate_estimate: req.body.graduate_estimate
// 	}, (req.query.schoolId || null) as string);

// 	if (community.email) {
// 		try {
// 			await sendEmailTemplate(REGISTRATION_EMAIL_TEMPLATE, community.email, mapCommunityToEmailData(community));
// 		} catch (error) {
// 			const errorMessage = `Error while sending email to ${community.email}. Error: ${(<any>error).message}`;
// 			console.error(errorMessage, error);
// 		}
// 	}

// 	if (req.query.html == 'true') {
// 		return res.send(`<p class="rf-H3">Вы успешно зарегистрированы!</p><br>
// 		<p class="rf-subheader">Скопируйте и сохраните вашу ссылку на опрос сообщества:</p><br>
// 		<a href="https://concept.rybakovfoundation.ru/diag/${community.id}" target="_blank" class="rf-link">https://concept.rybakovfoundation.ru/diag/${community.id}</a>
// 		<br>
// 		<br>
// 		<p class="rf-subheader">И ссылку на результаты опроса:</p><br>
// 		<a href="https://concept.rybakovfoundation.ru/diag/stats?c=${community.id}" target="_blank" class="rf-link">https://concept.rybakovfoundation.ru/diag/stats?c=${community.id}</a>
// 		<br>`);
// 	}
// 	return res.json({ message: `Ваша ссылка на опрос сообщества: https://concept.rybakovfoundation.ru/diag/${community.id}`, result: 'ok', id: community.id });

// });

// app.get('/', async (req: Request, res: Response)	 => {
// 	const communitiesRepository = new CommunitiesRepository(db);
// 	const community = await communitiesRepository.getById(req.query.id as string, true);
// 	return res.json({ result: community });
// });


async function migrate() {
	const communities: Record<string, ICommunityDB> = (await db.ref(PATHS.communities).once('value')).val();
	const set = new Set();
	//Object.values(communities).map(c => c.email).filter(e => !!e)
	const str = Object.values(communities)
		.filter(c => !!c.email)
		.map((c: any) => {
			if (!set.has(c.email.toLowerCase())) {
				set.add(c.email.toLowerCase())
				return [
					c.email, c.approx_community_size,
					`"${c.organization?.name?.short || c.organization?.name}"`,
					`"${c.organization?.address?.value || c.organization?.city}"`
				].join(',')
			}
			return '';
		}
		).filter(s => !!s)
		.join('\n')

	console.log(str);

	const statsSnap = await db.ref(PATHS.community_stats).once('value');
	const statsByCom = statsSnap.val();
	let count = 0
	Object.values(statsByCom).forEach((statsBySurvey: any) => {
		const respCount = Object.values(statsBySurvey).reduce((sum: number, s: any) => sum += s.count, 0);
		count += respCount;
	})
	console.log(count);
	// const answersSnap = await db.ref(PATHS.all_responses).once('value');
	// const answers = answersSnap.val();
	// console.log(answersSnap.numChildren());

	// const answers = require('../../../../rsa-integration-all_responses-export.json');

	// const update: any = {};

	// Object.entries(answers).forEach(([rid, response]: [string, any]) => {
	// 	if (response.community_id == 'PQYMDL') {
	// 		console.log(`${response.community_id}/${rid}`);
	// 	}
	// 	update[`${response.community_id}/${rid}`] = response._timestamp;
	// });


	// for (const comId in communities) {
	// 	if (Object.prototype.hasOwnProperty.call(communities, comId)) {
	// 		const com: ICommunityDB = communities[comId];
	// 		if (com?.organization?.data_type == 'dadata') {
	// 			continue;
	// 		}
	// 		const org: IManualOrganizationData = {
	// 			data_type: 'manual',
	// 			country: (com.country?.trim() || null) as string,
	// 			region: (com.region?.trim() || null) as string,
	// 			city: (com.city?.trim() || null) as string,
	// 			settlement: (com.settlement?.trim() || null) as string,
	// 			name: com.school?.trim() as string
	// 		}

	// 		update[`${PATHS.communities}/${comId}/organization`] = org;
	// 	}
	// }

	// console.log(update);
	// await db.ref(PATHS.responses_timeline).update(update);
}

// migrate().then(() => {
// 	console.log('ok');
// });


app.listen(5555, () => {
	console.log('Express app listening for port 5555');
});


module.exports = {
	app: app
};