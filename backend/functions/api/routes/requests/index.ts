import express = require('express');
import { Request, Response } from 'express';

import { db } from '../../../../database';
import { CommunitiesManager, RequestsManager } from '../../managers';
import { CommunitiesRepository, ResponsesRepository } from '../../repositories';

export const requestsRouter = express.Router();

/**
 * Подтверждает реквест (заявку) на
 * - изменение владельца аккаунта (с указанием почты)
 * - создание нового опроса для сообщества
 * Сюда попадают люди переходя по ссылке
 */
requestsRouter.get(`/:reqId/approve`, async (req: Request, res: Response) => {
	try {
		const reqId = req.params.reqId;

		if (!reqId) {
			throw new Error('Invalid RequestId');
		}

		const communitiesRepository = new CommunitiesRepository(db);
		const responsesRepository = new ResponsesRepository(db);
		const communitiesManager = new CommunitiesManager(communitiesRepository, responsesRepository);
		const requestsManager = new RequestsManager(db, communitiesManager);

		const request = await requestsManager.approveRequest(reqId);

		return res.redirect(`https://concept.rybakovfoundation.ru/diag/${request.data.com_id}`);
	} catch (ex) {
		console.error(ex);
		return res.status(400).json({
			result: 'error',
			error: (<Error>ex).message
		});
	}
});