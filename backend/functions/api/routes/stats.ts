import express = require('express');
import { accountingHandler } from '../../accounting';

export const statsRouter = express.Router();

/**
 * Пересчитывает статистику для новых ответов в базе
 */
statsRouter.get(`/calculate`, accountingHandler);