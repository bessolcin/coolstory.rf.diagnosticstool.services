import express = require('express');
import { Request, Response } from 'express';
import { checkCallbackParameters } from '../robokassa';
import { CommunitiesManager, PaymentsManager } from '../managers';
import { db } from '../../../database';
import { CommunitiesRepository, ResponsesRepository } from '../repositories';
import { REGISTRATION_EMAIL_TEMPLATE } from '../constants';
import { sendEmailTemplate, mapCommunityToEmailData } from '../services';
import { ICommunityDB } from '../../../../models';


export const paymentsRouter = express.Router();

let paymentsManager: PaymentsManager | undefined;
let communitiesManager: CommunitiesManager | undefined;

function getManager() {
	if (!paymentsManager) {
		paymentsManager = new PaymentsManager(db);
	}
	return paymentsManager;
}
function getComManager() {
	if (!communitiesManager) {
		communitiesManager = new CommunitiesManager(new CommunitiesRepository(db), new ResponsesRepository(db));
	}
	return communitiesManager;
}

paymentsRouter.post('/callback', async (req: Request, res: Response) => {
	const result = checkCallbackParameters(req.body as any);
	const payments = getManager();
	const communities = getComManager();

	if (result) {
		await payments.confirmPayment({ ...req.body, source: 'callback' });
		const communityDb = (await communities.getById(req.body.Shp_comid, false)) as unknown as ICommunityDB;

		if (communityDb?.email) {
			try {
				await sendEmailTemplate(REGISTRATION_EMAIL_TEMPLATE, communityDb.email, mapCommunityToEmailData(communityDb));
			} catch (error) {
				const errorMessage = `Error while sending email to ${communityDb.email}. Error: ${(<any>error).message}`;
				console.error(errorMessage, error);
			}
		}

		return res.send(`OK${req.body.InvId}`)
	}

	console.warn(`Incorrect signature`, req.body);
	return res.status(400);
});

paymentsRouter.post('/:paymentId', async (req: Request, res: Response) => {
	const paymentId = req.params.paymentId;
	const payments = getManager();
	await payments.confirmPayment({ InvId: paymentId, source: 'api' });
	return res.json({ result: 'ok' })
})

paymentsRouter.post('/', async (req: Request, res: Response) => {
	const payments = getManager();
	const communityId = req.body.community_id;

	if (!communityId) return res.status(400).json({ result: 'error', code: 'empty_community_id' });

	const payment = await payments.createPayment(communityId);
	await payments.bindWithCommunity(payment.id, communityId)

	return res.json(payment);
})