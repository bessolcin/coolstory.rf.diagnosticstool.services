import express = require('express');
import { communitiesRouter } from '../communities';
import { paymentsRouter } from '../payments';
import { requestsRouter } from '../requests';
import { statsRouter } from '../stats';

export const apiRouter = express.Router();

apiRouter.use('/api/v1/communities', communitiesRouter);
apiRouter.use('/api/v1/requests', requestsRouter);
apiRouter.use('/api/v1/payments', paymentsRouter);
apiRouter.use('/api/v1/stats', statsRouter);