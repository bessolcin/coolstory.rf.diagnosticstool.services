import express = require('express');
import { Request, Response, NextFunction } from 'express';

import { db } from '../../../../database';
import { CertificatesManager, CommunitiesManager, ICommunitySizeParams, IDTRequest, PaymentsManager, RequestsManager } from '../../managers';
import { ICreateCommunityRequest, ICreateCommunityResponse } from '../../../../../models/common';
import { CommunitiesRepository, ResponsesRepository } from '../../repositories';
import { HtmlToImageService } from '../../services/pdf';
import { ScreenshotService } from '../../services/pdf/screenshot.service';
import { getOrgName } from '../../../../../models';

import { REGISTRATION_EMAIL_TEMPLATE, REQUEST_EMAIL_TEMPLATES } from '../../constants';
import { sendEmailTemplate, mapCommunityToEmailData } from '../../services';
import { recalculateStatsForCommunity } from '../../../accounting';
import { getSignedPaymentUrl } from '../../robokassa';

export const communitiesRouter = express.Router();

const communitiesRepository = new CommunitiesRepository(db);
const responsesRepository = new ResponsesRepository(db);
const paymentsManager = new PaymentsManager(db);

function cacheResponse(req: Request, res: Response, next: NextFunction) {
	const cacheMinutes = 300;
	res.set('Cache-control', `public, max-age=${cacheMinutes * 60}, s-maxage=${cacheMinutes * 60}`);
	next();
}

/**
 * Создать сообщество
 */
communitiesRouter.post('/', async (req: Request, res: Response) => {
	try {
		const communitiesManager = new CommunitiesManager(communitiesRepository, responsesRepository);
		const request = <ICreateCommunityRequest>req.body;

		const community = await communitiesManager.createCommunity(request, (req.query.schoolId || null) as string);

		// TODO: Брать ид из пеймента и регать сообщество с ним, но сначала надо порешать с Димой
		if (!!request.payment_id && request.payment_id === process.env.REF_ID) {
			const payment = await paymentsManager.createPayment(community.id as string);
			const result = await paymentsManager.bindWithCommunity(payment.id, community.id as string);
			
			if (community?.email) {
				try {
					await sendEmailTemplate(REGISTRATION_EMAIL_TEMPLATE, community.email, mapCommunityToEmailData(community));
				} catch (error) {
					const errorMessage = `Error while sending email to ${community.email}. Error: ${(<any>error).message}`;
					console.error(errorMessage, error);
				}
			}
			return res.json(<ICreateCommunityResponse>{ message: `Ваша ссылка на опрос сообщества: https://concept.rybakovfoundation.ru/diag/${community.id}`, result: 'ok', id: community.id });
		}
		else {
			const payment = await paymentsManager.createPayment(community.id as string);
			const paymentUrl = getSignedPaymentUrl(payment.id, community.id as string);
			return res.json({ payment_url: encodeURI(paymentUrl) });
		}

		if (req.query.html == 'true') {
			return res.send(`<p class="rf-H3">Вы успешно зарегистрированы!</p><br>
			<p class="rf-subheader">Скопируйте и сохраните вашу ссылку на опрос сообщества:</p><br>
			<a href="https://concept.rybakovfoundation.ru/diag/${community.id}" target="_blank" class="rf-link">https://concept.rybakovfoundation.ru/diag/${community.id}</a>
			<br>
			<br>
			<p class="rf-subheader">И ссылку на результаты опроса:</p><br>
			<a href="https://concept.rybakovfoundation.ru/diag/stats?c=${community.id}" target="_blank" class="rf-link">https://concept.rybakovfoundation.ru/diag/stats?c=${community.id}</a>
			<br>`);
		}
	} catch (ex) {
		const e = <Error>ex;
		console.error(e.message);
		return res.status(400).json(<ICreateCommunityResponse>{ result: 'error', error: e.message });
	}
});

/**
 * Получить сообщество
 */
communitiesRouter.get('/:comId', cacheResponse, async (req: Request, res: Response) => {
	const community = await communitiesRepository.getById(req.params.comId, req.query.stats === 'true');
	return res.json({ result: community });
});

communitiesRouter.get(`/:comId/surveys/activity`, cacheResponse, async (req: Request, res: Response) => {
	const comId = req.params.comId;

	if (!comId) {
		return res.json({ error: 'Community ID is empty' });
	}

	const communitiesManager = new CommunitiesManager(communitiesRepository, responsesRepository);
	const activityData = await communitiesManager.getActivity(comId)

	return res.json(activityData);
});

communitiesRouter.get(`/:comId/surveys/recalculate`, cacheResponse, async (req: Request, res: Response) => {

	const comId = req.params.comId;

	if (!comId) {
		return res.json({ error: 'Community ID is empty' });
	}

	await recalculateStatsForCommunity(comId)
	const community = await communitiesRepository.getById(req.params.comId, true);

	return res.json(community)
})

/**
 * Получить сертификат для сообщества по опросу
 */
communitiesRouter.get(`/:comId/surveys/:surveyId/certificate`, async (req: Request, res: Response) => {
	const comId = req.params.comId;
	const surveyID = req.params.surveyId;

	if (!comId) {
		return res.json({ error: 'Community ID is empty' });
	}
	if (!surveyID) {
		return res.json({ error: 'Survey ID is empty' });
	}

	const screenshotService = new ScreenshotService({});
	const htmlToImageService = new HtmlToImageService(screenshotService);
	const certificatesManager = new CertificatesManager(communitiesRepository, htmlToImageService);

	const certificate = await certificatesManager.createCertificate(comId, surveyID)

	if (certificate) {
		res.writeHead(200, {
			'Content-Type': 'application/pdf',
			'Content-Disposition': `attachment; filename="${certificate.filename}.pdf"`
		});
		return res.end(certificate.buffer);
	}
	else {
		return res.json({ error: 'Community not found' });
	}
});

communitiesRouter.post(`/:comId/size`, async (req: Request, res: Response) => {
	const communitiesManager = new CommunitiesManager(communitiesRepository, responsesRepository);
	const size = req.body as ICommunitySizeParams;
	if (typeof size.graduate_estimate !== 'number' || typeof size.students_count !== 'number' || typeof size.teachers_count !== 'number' || !req.params.comId)
		return res.status(400);

	const newSize = await communitiesManager.changeCommunitySize(req.params.comId, req.body);

	return res.json(newSize);
})


/**
 * Создает реквест (заявку) на
 * - изменение владельца аккаунта (с указанием почты)
 * - создание нового опроса для сообщества
 */
communitiesRouter.post(`/:comId/:requestType`, async (req: Request, res: Response) => {
	const communitiesManager = new CommunitiesManager(communitiesRepository, responsesRepository);

	const requestsManager = new RequestsManager(db, communitiesManager);

	try {
		let comId = req.params.comId;
		if (comId == 'from_form') {
			comId = req.body.comId;
		}

		// TODO: move logic to requestsManager
		const community = await communitiesRepository.getById(comId);

		if (!community) {
			return res.status(400).json({ result: 'error', error: 'community_not_found' });
		}

		let request: IDTRequest | null = null;

		switch (req.params.requestType) {
			case 'surveys':
				request = await requestsManager.createSurveyRequest(comId);
				break;
			case 'change_owner':
				const newOwnerData = req.body;
				request = await requestsManager.createOwnerChangeRequest(comId, community.email, newOwnerData.name, newOwnerData.email);
				break;
			default:
				break;
		}

		if (!request) {
			return res.status(400).json({ result: 'error', error: 'community_not_found', messgae: 'Неизвестный тип запроса' });
		}

		const emailTemplateId = REQUEST_EMAIL_TEMPLATES[request.type];
		if (emailTemplateId) {
			const emailResp = await sendEmailTemplate(emailTemplateId, community.email, {
				school_id: comId,
				school_name: getOrgName(community.organization),
				approve_url: `https://concept.rybakovfoundation.ru/diag/api/community/api/v1/requests/${request.id}/approve`,
				user_name: community.name,

				...request.data
			});
			console.log(emailResp);
		}
		return res.json({ result: 'OK', message: 'OK' });
	} catch (ex) {
		console.error(`Error handling DT request type ${req.params.requestType} for ${req.params.comId}: ${(<any>ex).message}`, ex);
		return res.status(400).json({ result: 'error', error: 'community_not_found' });
	}
});