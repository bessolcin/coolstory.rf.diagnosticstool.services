import { database, initializeApp, credential } from 'firebase-admin';
import * as config from './coolstoryprointegration-firebase-adminsdk-32r3r-d26e4fa347.json';
import * as configRf from './rsa-analytics-334007-acac2f849446.json';

const coolStoryApp = initializeApp({
	credential: credential.cert(<any>config),
	databaseURL: 'https://rsa-integration.firebaseio.com/',
	projectId: 'coolstoryprointegration'
}, 'coolstory');
const rfApp = initializeApp({
	credential: credential.cert(<any>configRf),
	databaseURL: 'https://rsa-analytics-334007-default-rtdb.europe-west1.firebasedatabase.app/',
	projectId: 'rsa-analytics-334007'
}, 'rf');

export const db: database.Database = database(rfApp);
export const dbRF: database.Database = database(rfApp);