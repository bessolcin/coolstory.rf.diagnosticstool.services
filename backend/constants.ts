export const COMMON_HEADERS = {
	'Access-Control-Allow-Origin': '*',
	'Content-Type': 'application/json'
};

export const DEFAULT_CORS_RESPONSE_YC = {
	"statusCode": 200,
	"headers": {
		'Content-Type': 'application/json',
		'Access-Control-Allow-Origin': '*',
		'Access-Control-Allow-Methods': 'GET',
		'Access-Control-Allow-Headers': 'Content-Type',
		'Access-Control-Max-Age': '3600'
	},
	"body": "",
	"isBase64Encoded": false
}

export interface IYandexEvent {
	httpMethod: string;

	queryStringParameters: { [key: string]: string };

	multiValueQueryStringParameters: { [key: string]: string[] };

	body: string;
}