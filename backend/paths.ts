export const PATHS = {
	communities: 'diagnostics_tool/communities',
	all_responses: 'diagnostics_tool/all_responses',
	community_responses: 'diagnostics_tool/community_responses',
	community_stats: 'diagnostics_tool/community_stats',
	max_values: 'diagnostics_tool/community_max_values',
	requests: 'diagnostics_tool/requests',
	responses_timeline: 'diagnostics_tool/responses_timeline',
	payments: 'diagnostics_tool/payments',
};