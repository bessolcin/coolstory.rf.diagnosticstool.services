# CoolStory.RF.DiagnosticsTool.Services


```
POST /diagnostics/survey - сохраняет ответ на опрос для школы
ошибки: 400 - слишком частый запрос (делаем проверку, чтобы не чаще чем 1 раз в 10 минут)

POST /diagnostics/community - создает опрос для школы
GET /diagnostics/community/:id - получает данные опроса по школе

POST /diagnostics/stats
```