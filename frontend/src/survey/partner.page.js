$(function () {
	const survey = require('./partner.json');

	if (!window.surveysByRole) {
		window.surveysByRole = {};
	}

	window.surveysByRole.partner = survey;
});