$(function () {
	const survey = require('./student.json');

	if (!window.surveysByRole) {
		window.surveysByRole = {};
	}

	window.surveysByRole.student = survey;
});