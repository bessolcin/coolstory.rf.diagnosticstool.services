import { isMobile } from '../dashboard/utils';
import { GLOBAL_PATH } from '../js/path';
// import s from './models/teacher.json';

declare const Survey: any;
declare const anime: any;
declare const ga: any;
declare const ym: any;

const RF_TIMESTAMP_KEY = '_rf_timestamp';
const RF_LIGHT_VERSION_SURVEY = 'light_version';

interface ISurveyParams {
	role: string | null;
	school_id: string | null;
	isLight: boolean;
	locale: string;
}

$(function () {
	const SAVE_RESPONSE_PAUSE = 10;
	let timerId = 0;

	// let doAnimantion = true;
	// const animationTime = 400;

	const surveyParams = getSurveyParams(window.location.search);

	if (!surveyParams.role) {
		window.location.href = `${GLOBAL_PATH}/role?c=${surveyParams.school_id}`;
		return;
	}
	const currentSurveyStorageName = `rf_dt_survey_state_${surveyParams.role}${surveyParams.isLight ? '_light' : ''}`;

	startSurvey(surveyParams);

	async function startSurvey(surveyParams: ISurveyParams) {
		try {
			const surveyModel = await loadRoleSurvey(surveyParams.isLight ? RF_LIGHT_VERSION_SURVEY : surveyParams.role as string);
			const survey = initSurvey(surveyModel, surveyParams.locale);
			timerId = enableAutoSave(survey);

			var surveyContainer = $('#survey_container');
			(<any>surveyContainer).Survey({ model: survey });

			if (isMobile()) {
				surveyContainer.addClass('mobile');
			}
		} catch (e) {
			alert('Возникла непредвиденная ошибка. Пожалуйста попробуйте обновить страницу или обратитесь к нам в поддержку.');
			console.log(e);
		}
	}

	function getSurveyParams(search: string): ISurveyParams {
		const currentParams = new URLSearchParams(search);
		return {
			role: currentParams.get('role'),
			school_id: currentParams.get('c'),
			isLight: currentParams.get('light') === 'true',
			locale: currentParams.get('lang') || 'ru'
		};
	}

	function loadRoleSurvey(surveyName: string) {
		// return s;
		return fetch(`https://storage.yandexcloud.net/rf-diagnosticstool-static/v3/${surveyName}.json`)
			.then(r => r.json());
	}

	function initSurvey(surveyModel: any, locale: string) {

		Survey.StylesManager.applyTheme('defaultV2');
		const survey = new Survey.Model(surveyModel);

		const surveyState = loadState(currentSurveyStorageName);
		if (surveyState) {
			if (surveyState.currentPageNo) survey.currentPageNo = surveyState.currentPageNo;
			if (surveyState.data) survey.data = surveyState.data;
		}

		survey.locale = locale;
		survey.onCurrentPageChanged.add(onSurveyPageChanged);
		survey.onComplete.add(onSurveyComplete);

		return survey;
	}

	function enableAutoSave(survey: any) {
		return window.setInterval(function () {
			var surveyState = {
				currentPageNo: survey.currentPageNo,
				data: survey.data
			};
			saveState(currentSurveyStorageName, surveyState);
		}, 30 * 1000);
	}

	function onSurveyPageChanged(sender: any) {
		saveState(currentSurveyStorageName, {
			currentPageNo: sender.currentPageNo,
			data: sender.data
		});
		// animate('slideDown', animationTime);

		analyticsPageView(`/diag/survey/${surveyParams.role}/${sender.currentPageNo}`);
	}

	function onSurveyComplete(sender: any, options: any) {
		analyticsPageView(`/diag/survey/${surveyParams.role}/complete`);
		analyticsEvent({ name: `survey_complete_${surveyParams.role}`, category: 'survey', type: 'survey_complete', });

		//kill the timer
		clearInterval(timerId);
		//save the data on survey complete. You may call another function to store the final results
		saveState(currentSurveyStorageName, {
			currentPageNo: sender.currentPageNo,
			data: sender.data
		});

		const successUrl = `${GLOBAL_PATH}/success?c=${surveyParams.school_id}&role=${surveyParams.role}${surveyParams.isLight ? '&light=true' : ''}`;
		if (isDemoCommunitySurvey(surveyParams.school_id)) {
			window.location.href = successUrl
			return;
		}

		if (canSendResponseAfterTime()) {
			sendDataToServer(sender)
				.done((response) => {
					if (response.result == 'ok') {
						saveState(RF_TIMESTAMP_KEY, (new Date()).getTime());
						saveState(currentSurveyStorageName, null);
						console.log('Response saved.');

						window.location.href = successUrl;
					}
					else {
						alert('ОЙ! При сохранении данных опроса произошла ошибка. Пожалуйста, попробуйте нажать кнопку Готово через несколько минут.');
					}
				});
		}
		else {
			analyticsEvent({
				name: `survey_repeatanswer_${surveyParams.role}`,
				category: 'survey',
				type: 'survey_complete'
			});
		}
	}

	function isDemoCommunitySurvey(school_id: string | null) {
		return school_id == '5OKMRL' || school_id == 'LCJYD9';
	}

	function canSendResponseAfterTime() {
		const requestTimestamp = loadState(RF_TIMESTAMP_KEY);
		let diff = -1;
		if (requestTimestamp) {
			const now = (new Date()).getTime();
			diff = Math.round(Math.abs(now - requestTimestamp) / (1000 * 60));
		}

		return diff == -1 || diff > SAVE_RESPONSE_PAUSE;
	}

	function sendDataToServer(survey: any) {
		return $.ajax({
			method: 'POST',
			url: '/diag/api/response',
			dataType: 'json',
			contentType: "application/json",
			data: JSON.stringify({
				school_id: surveyParams.school_id,
				survey_role: surveyParams.role,
				light: surveyParams.isLight,
				survey_response: survey.data
			})
		});
	}

	// function onSurveyPageChanging(sender: any, options: any) {
	// 	if (!doAnimantion) { return; }

	// 	options.allowChanging = false;

	// 	setTimeout(function () {
	// 		doAnimantion = false;
	// 		sender.currentPage = options.newCurrentPage;
	// 		doAnimantion = true;
	// 	}, animationTime);
	// 	animate('slideUp', animationTime);
	// }

	// function onSurveyCompleting(sender: any, options: any) {
	// 	if (!doAnimantion) {
	// 		return;
	// 	}
	// 	options.allowComplete = false;
	// 	setTimeout(function () {
	// 		doAnimantion = false;
	// 		sender.doComplete();
	// 		doAnimantion = true;
	// 	}, animationTime);
	// 	animate('slideUp', animationTime);
	// }

	// function animate(animationType: 'slideDown' | 'slideUp', duration: number) {
	// 	if (!duration) { duration = 1000; }

	// 	var animation = {
	// 		targets: '.sv-page', //'.sv-question',
	// 		duration: duration,
	// 		easing: 'easeInQuad',
	// 		opacity: 0
	// 	};
	// 	if (animationType == 'slideDown') {
	// 		animation.opacity = 1;
	// 	}
	// 	if (animationType == 'slideUp') {
	// 		animation.opacity = 0;
	// 	}

	// 	anime(animation);
	// }

});

function loadState(storageKey: string) {
	//Here should be the code to load the data from your database
	var stateStr = null;
	if (window.localStorage) {
		stateStr = window.localStorage.getItem(storageKey) || null;
	}

	var state = null;
	if (stateStr) {
		try {
			state = JSON.parse(stateStr);
		}
		catch (e) {
			console.error(`State under key ${storageKey} is corrupted: ${stateStr}`);
		}
	}
	return state;
}

function saveState(key: string, object: any) {
	try {
		if (window.localStorage) {
			if (object == null) {
				window.localStorage.removeItem(key);
				return;
			}
			window.localStorage.setItem(key, JSON.stringify(object));
		}
	} catch (error) {
		console.error(error);
	}
}

function analyticsPageView(path: string) {
	try {
		ym && ym(85017238, 'hit', path);
		ga && ga('send', 'pageview', path);
	} catch (ex) {
		console.log(ex);
	}
}

function analyticsEvent(e: any) {
	try {
		ym && ym(85017238, 'reachGoal', e.name);
		ga && ga('send', 'event', e.category, e.type, e.name);
	} catch (ex) {
		console.log(ex);
	}
}
