$(function () {
	const survey = require('./teacher.json');

	if (!window.surveysByRole) {
		window.surveysByRole = {};
	}

	window.surveysByRole.teacher = survey;
});