$(function () {
	const survey = require('./parent.json');

	if (!window.surveysByRole) {
		window.surveysByRole = {};
	}

	window.surveysByRole.parent = survey;
});