$(function () {
	const survey = require('./graduate.json');

	if (!window.surveysByRole) {
		window.surveysByRole = {};
	}

	window.surveysByRole.graduate = survey;
});