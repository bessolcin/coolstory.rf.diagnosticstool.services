import { ICommunityFullModel } from '../../../models';
import { rfCommunityStore } from '../dashboard/components';

import { SURVEY_ROLES_LABELS } from '../dashboard/constants';

declare const Alpine: any;

const roles = [
	'teacher',
	'parent',
	'graduate',
	'partner',
	'student',
];
const IMG_URL = 'https://storage.yandexcloud.net/rf-diagnosticstool-static/images';

document.addEventListener('alpine:init', () => {

	Alpine.store('community', rfCommunityStore(false));

	Alpine.data('roles', () => (
		{
			roles: [] as any[],
			init() {
				Alpine.effect(() => {
					const communityStore = Alpine.store('community');
					const community: ICommunityFullModel = communityStore.value;
					const isLight = communityStore.isLight;

					if (community) {
						this.roles = roles.map(r => ({
							label: SURVEY_ROLES_LABELS[r],
							icon: `${IMG_URL}/${r}.svg`,
							code: r,
							link: `/diag/survey?c=${community.id}&role=${r}${isLight ? '&light=true' : ''}`
						}));
					}

				})
			}
		}
	));

});