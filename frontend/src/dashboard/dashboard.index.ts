import $ from 'jquery'
import { ICommunityFullModel, isDadataOrg, isManualOrg, ISurvey } from '../../../models';
import { GLOBAL_PATH } from '../js/path';
import { rfCommunityStore, rfDirectionsComponent, rfDirectionTaskRoles, rfDirectionTasks, rfRolesChart, rfSurveyDropdown, rfSurveyStats } from './components';
import { DailyActivity, rfActivityStore } from './components/activity.store';
import { rfDirectionsStore } from './components/directions.store';
import { COMMUNITY_API_URL, DATA_LABELS_COLOR, DIRECTIONS, DIRECTION_LABELS, fontSetting, LEVELS_WITH_DIRECTION, ROLE_COLORS, SURVEY_COLORS } from './constants';
import { isMobile } from './utils';

declare const Alpine: any;
declare const Chart: any;


document.addEventListener('alpine:init', () => {

	if (window['Chart' as any]) {
		Chart.defaults.font.family = "'PN','Arial','sans-serif'";
		Chart.defaults.font.weight = '600';
	}

	const needLoadStats = window.location.pathname.includes('diag/stats');

	Alpine.store('community', rfCommunityStore(needLoadStats));

	Alpine.store('survey_index', {
		index: 0,
		init() {
			Alpine.effect(() => {
				const community = Alpine.store('community').value;
				if (community?.surveys) {
					this.index = community.surveys.length - 1;
				}
			});
		},
		update(newIndex: number) {
			this.index = newIndex;
		}
	});

	Alpine.store('survey_index_comparabale', {
		index: 0,
		is_last_survey: false,
		init() {
			Alpine.effect(() => {
				const community = Alpine.store('community').value;
				const currentIndex = Alpine.store('survey_index').index;

				if (community?.surveys) {
					const lastIndex = community.surveys.length - 1;

					if (currentIndex == lastIndex) {
						if (lastIndex - 1 >= 0) {
							this.index = lastIndex - 1;
							this.is_last_survey = false;
						}
						else {
							this.index = lastIndex;
							this.is_last_survey = true;
						}
					}
					else {
						this.index = lastIndex;
						this.is_last_survey = true;
					}
				}
			});
		},
		update(newIndex: number) {
			this.index = newIndex;
		}
	});

	Alpine.store('directions', rfDirectionsStore());

	Alpine.data('dropdown', rfSurveyDropdown);

	Alpine.data('survey_stats', rfSurveyStats);

	Alpine.data('rf_dashboard', () => ({
		initialized: false,
		community: null as ICommunityFullModel | null,
		fixed: false,
		init() {
			Alpine.effect(() => {
				const community: ICommunityFullModel = Alpine.store('community').value;
				if (community && !this.initialized) {
					this.initPageView(community);
					this.community = community;
					this.initialized = true;
				}
			});
		},
		toggle(val: any) {
			this.fixed = val < 0;
			// console.log(val);
		},
		getCommunityName() {
			return ((<any>this.community?.organization?.name)?.short_with_opf || this.community?.organization?.name || 'Название') + ', ' + (this.community?.city || 'населенный пункт');
		},
		initPageView(community: ICommunityFullModel) {

			// подставляет данные на страницу
			// const totalSample = community.sample_count.student + community.sample_count.teacher + community.sample_count.parent + community.sample_count.graduate;
			if (isManualOrg(community.organization)) {
				const city = community.organization.city ? community.organization.city : community.organization.settlement
				community.city = city;
				community.organization.city = city;
			}
			else if (isDadataOrg(community.organization)) {
				const address = community.organization.address.data;
				community.city = address.city ? `${address.city_type} ${address.city}` : `${address.settlement_type} ${address.settlement}`;
			}

			replaceData(community);
			// replaceData({
			// 	community_short_name: community.organization.name.length > 50 ? community.organization.name.substring(0, 50) + '…' : community.organization.name,
			// 	total_count: community.stats?.count,
			// 	total_percent: community.stats ? `${Math.min(Math.round(community.stats.count / community.sample_count.total * 100), 100)}%` : '- %',
			// 	total_sample: totalSample,

			// 	community_size_70: Math.round(community.approx_community_size * 0.7),
			// });
			const url = new URL(window.location.href);
			initShareButton('rf_share_survey', `${url.origin}${GLOBAL_PATH}/stats?c=${url.searchParams.get('c')}`);
			initShareButton('rf_share_survey1', `${url.origin}${GLOBAL_PATH}/${url.searchParams.get('c')}`);

			// community.certificate_url = `https://concept.rybakovfoundation.ru/diag/api/community/api/v1/communities/${community.id}/certificate`;
			initChart(community);
			initNewSurveyButton(community);

			$('input[name=comId]').val(community.id);
		}
	})
	);

	Alpine.data('community', () => {
		return {
			community: null as (ICommunityFullModel | null),
			init() {
				Alpine.effect(() => {
					const community: ICommunityFullModel = Alpine.store('community').value;
					if (community) {
						this.community = community;
					}
				});
			}
		};
	});

	Alpine.data('directions', rfDirectionsComponent);

	Alpine.data('chart_by_roles', rfRolesChart);

	Alpine.data('directiontasks', rfDirectionTasks);

	Alpine.data('direction_task_roles', rfDirectionTaskRoles);


	if (!needLoadStats) {
		return;
	}
	Alpine.store('survey_activity', rfActivityStore());

	let chart: null | any = null;
	Alpine.data('survey_dynamics', (id: string, chartEl: HTMLElement) => ({
		enabled: true,
		id,
		chartEl,
		config: <any>{
			type: 'bar',
			responsive: true,
			// plugins: [ChartDataLabels],
			options: {
				barThickness: 4,
				borderWidth: 0,
				borderColor: 'rgba(0,0,0,0)',
				aspectRatio: 1.2,
				maintainAspectRatio: false,
				// indexAxis: 'y',
				scales: {
					y: {
						ticks: {
							...fontSetting,
							stepSize: 1
						},
						title: {
							display: true,
							text: 'Количество ответов'
						},
					},
					x: {
						// max: 100,
						beginAtZero: true,
						ticks: {
							...fontSetting,
							font: {
								size: 10
							},
						}
					}
				},
				elements: {
					bar: {
						borderRadius: 8
					}
				},
				plugins: {
					// Change options for ALL labels of THIS CHART
					datalabels: {
						anchor: 'end',
						align: 'left',
						clamp: true,
						offset: -30,
						labels: {
							title: {
								font: {
									weight: 'bold',
								},
								color: DATA_LABELS_COLOR
							}
						}
					},
					legend: {
						display: false,
					},
					tooltip: {
						enabled: true
					}
				}
			},
		},
		init() {
			Alpine.effect(() => {
				const activity = Alpine.store('survey_activity').value;
				const community: ICommunityFullModel = Alpine.store('community').value;
				const surveyIndex = Alpine.store('survey_index').index;
				
				// console.log(activity);

				if (activity && community.surveys && community.surveys[surveyIndex]) {
					const surveyId = community.surveys[surveyIndex].id as string;
					
					const surveyActivitydata = (<Record<string, DailyActivity[]>>activity)[surveyId];

					if (!surveyActivitydata) return;

					if (!chart) {
						this.config.data = this.getData(surveyActivitydata);
						chart = new Chart(this.chartEl, this.config);
					}
					else {
						const data = this.getData(surveyActivitydata);

						const currentDataArray: number[] = chart.data.datasets[0].data;
						currentDataArray.splice(0, currentDataArray.length);
						currentDataArray.push(...data.datasets[0].data);

						const currentLabels: string[] = chart.data.labels;
						currentLabels.splice(0, currentLabels.length);
						currentLabels.push(...data.labels);

						chart.update();
					}
				}
			});
		},
		getData(activity: DailyActivity[]) {
			const limit = -30;
			return {
				labels: activity.slice(limit).map(d => d.date),
				datasets: [
					{
						label: '',
						data: activity.slice(limit).map(d => d.count),
						backgroundColor: ROLE_COLORS.student.backgroundColor
					}
				]
			}
		}
	}))
});

// export function showCommunityDataOnPage(params: any) {
// 	const communitylId = params.get('c');
// 	if (!communitylId) {
// 		window.location.href = '/diag';
// 	}

// 	const commState = loadState(communitylId);
// 	if (commState) {
// 		initPageView(commState.value);
// 	}
// 	else {
// 		getCommunityData(communitylId)
// 			.done((response: { result: ICommunityFullModel }) => {
// 				if (response?.result?.id) {
// 					saveState(communitylId, {
// 						value: response.result,
// 						timestamp: (new Date()).getTime()
// 					});

// 					initPageView(response.result);
// 				}
// 				else {
// 					window.location.href = '/diag';
// 				}
// 			});
// 	}
// }

/*
export function initPageView(community: ICommunityFullModel) {
	setTimeout(function () {

		// подставляет данные на страницу
		const totalSample = community.sample_count.student + community.sample_count.teacher + community.sample_count.parent + community.sample_count.graduate;
		community.city = community.city ? community.city : community.settlement;

		replaceData(community);
		replaceData({
			community_short_name: community.organization.name.length > 50 ? community.organization.name.substring(0, 50) + '…' : community.organization.name,
			total_count: community.stats?.count,
			total_percent: community.stats ? `${Math.min(Math.round(community.stats.count / community.sample_count.total * 100), 100)}%` : '- %',
			total_sample: totalSample,

			community_size_70: Math.round(community.approx_community_size * 0.7),
		});
		initShareButton();

		// community.certificate_url = `https://concept.rybakovfoundation.ru/diag/api/community/api/v1/communities/${community.id}/certificate`;
		initChart(community);
		document.addEventListener('alpine:init', () => {
			Alpine.store('community').update(community);
		})
		initNewSurveyButton(community);

		$('input[name=comId]').val(community.id);
	}, 100);
}
*/

export function initShareButton(buttonId: string, url: string) {
	try {
		if (window['Ya' as any]) {
			(<any>window['Ya' as any]).share2(buttonId, {
				content: {
					url
				},
				theme: {
					// curtain: true,
					popupPosition: isMobile() ? 'outer' : 'inner'
				}
			});
		}
	} catch (error) {
		console.error(error);
	}
}

export function initChart(community: ICommunityFullModel | null) {
	const CHART_BACKGROUND_COLOR = '#FFEC8F';
	if (!community || !community?.surveys) {
		// console.log('surveys are empty');
		return;
	}

	const surveys: ISurvey[] = community.surveys;

	const labels = DIRECTIONS.map(d => [`${surveys[surveys.length - 1].stats?.directions[d].index ?? ' - '}%`, ...DIRECTION_LABELS[d]]);

	const data = {
		labels: labels,
		datasets: [
			{
				label: LEVELS_WITH_DIRECTION[1].label,
				backgroundColor: LEVELS_WITH_DIRECTION[1].backgroundColor,
				borderColor: LEVELS_WITH_DIRECTION[1].borderColor,
				data: DIRECTIONS.map(d => LEVELS_WITH_DIRECTION[1].data[d]),
				pointRadius: 1,
				borderWidth: 0
			},
			{
				label: LEVELS_WITH_DIRECTION[0].label,
				backgroundColor: LEVELS_WITH_DIRECTION[0].backgroundColor,
				borderColor: LEVELS_WITH_DIRECTION[0].borderColor,
				data: DIRECTIONS.map(d => LEVELS_WITH_DIRECTION[0].data[d]),
				pointRadius: 1,
				borderWidth: 0
			},

		]
	};

	const sortedDatasets = community.surveys
		.map((s: any, i: number) => {
			return <any>{
				label: `Опрос №${i + 1} от ${s.start_date_str}`,
				data: DIRECTIONS.map(d => s.stats.directions[d].index),
				borderColor: SURVEY_COLORS[i % SURVEY_COLORS.length].borderColor,
				backgroundColor: (i == surveys.length - 1) ? SURVEY_COLORS[i % SURVEY_COLORS.length].backgroundColor : 'rgba(0,0,0,0)'
				// labels: DIRECTIONS.map(d => [`${s.stats.directions[d].index}%`, ...DIRECTION_LABELS[d]]),
			};
		}).reverse();
	data.datasets.unshift(...sortedDatasets);

	const plugin = {
		id: 'custom_canvas_background_color',
		beforeDraw: (chart: any) => {
			const ctx = chart.canvas.getContext('2d');
			ctx.save();
			ctx.globalCompositeOperation = 'destination-over';
			ctx.fillStyle = CHART_BACKGROUND_COLOR;
			ctx.fillRect(0, 0, chart.width, chart.height);
			ctx.restore();
		}
	};

	const config = {
		type: 'radar',
		data,
		options: {
			responsive: true,
			scales: {
				r: {

					max: Math.round(Math.max(...data.datasets.map(d => d.data).flat()) / 10) * 10 + 20,
					min: 0,
					ticks: {
						// Include a dollar sign in the ticks
						callback: function (value: any, index: number, values: any) {
							return value + '%';
						},
						backdropColor: 'rgba(255,255,255,0.1)',
						font: {
							size: 14
						}
					}
				}
			},
			plugins: {
				legend: {
					display: true
				},
				tooltip: {
					backgroundColor: '#fff',
					titleColor: '#000',
					bodyColor: '#000'
				}
			},
			elements: {
				line: {
					tension: 0.3,
					backgroundColor: 'rgba(241, 201, 207, 0.5)',
					borderColor: '#FF7AAF',
					borderWidth: 3
				},
				point: {
					radius: 5,
					hoverRadius: 8,
					backgroundColor: '#FF7AAF'
				}
			}
		},
		plugins: [plugin]
	};

	const chartContainer = document.getElementById('directions_radar_chart');
	if (chartContainer) {
		const myChart = new Chart(chartContainer, config);

		setTimeout(() => {
			const a = $('.rf-chart-download > a');
			a.attr('download', 'chart.png');
			a.attr('href', myChart.toBase64Image());
		}, 300);
	}
}



function initNewSurveyButton(community: ICommunityFullModel) {
	const emailParts = community.email.split('@');
	const maskedEmail = `${emailParts[0].substring(0, 3)}***${emailParts[0].substring(emailParts[0].length - 3)}@${emailParts[1]}`;

	const description = [$('#rec421555788 .t-descr strong'), $('#rec431478214 .t702__descr p:last')];
	description.forEach(el => {
		if (el.length) {
			el.html(el.html().replace('%%community_email%%', maskedEmail)); // фыва
		}
	})

	$('#rec421555788 .t390__btn_first').click(function () {

		$.ajax({
			method: 'POST',
			url: `${COMMUNITY_API_URL}/${community.id}/surveys`,
			contentType: "application/json; charset=utf-8",
			error: (e: any) => {
				console.error(e);
			}
		}).done((response: any) => {
			alert('На почту отправлено письмо для подтверждения запуска следующего опроса.');
		});

	});
}

function replaceData(data: Record<string, any>) {
	for (const key in data) {
		if (Object.hasOwnProperty.call(data, key)) {
			const value = data[key];
			$(`[data-replace-key=${key}]`).text(value);
		}
	}
}