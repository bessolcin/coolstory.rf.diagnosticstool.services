import { fontSetting, DATA_LABELS_COLOR, ROLE_CODES, ROLE_LABELS, DIRECTION_COLORS } from '../constants';
import { IDirectionTask, ITaskData } from './directions.store';

declare const Chart: any;
declare const Alpine: any;
declare const ChartDataLabels: any;

export function rfDirectionTaskRoles(directionCode: string, taskCode: string, chartId: string) {
	let diffs: (number | null)[] = [];
	let chart: null | any = null;

	return {
		directionCode,
		taskCode,
		chartId,
		chartEl: null as HTMLElement | null,
		opened: false,
		direction: null as IDirectionTask | null,
		task: null as ITaskData | null,
		config: <any>{
			type: 'bar',
			responsive: true,
			plugins: [ChartDataLabels],
			options: {
				barThickness: 30,
				borderWidth: 3,
				borderColor: 'rgba(0,0,0,0)',
				aspectRatio: 0.5,
				maintainAspectRatio: false,
				indexAxis: 'y',
				scales: {
					y: {
						ticks: fontSetting
					},
					x: {
						max: 100,
						beginAtZero: true,
						ticks: fontSetting
					}
				},
				elements: {
					bar: {
						borderRadius: 8
					}
				},
				plugins: {
					// Change options for ALL labels of THIS CHART
					datalabels: {
						anchor: 'end',
						align: 'left',
						clamp: true,
						offset: -30,
						labels: {
							title: {
								font: {
									weight: 'bold',
								},
								color: DATA_LABELS_COLOR
							}
						},
						formatter: function (value: any, context: any) {
							// console.log(arguments)
							return context.dataset.data[context.dataIndex] ? `${context.dataset.data[context.dataIndex]}%      ${<number>diffs[context.dataIndex] >= 0 ? '+' : '−'}${Math.abs(<number>diffs[context.dataIndex])}%` : '    *';
						}
					},
					legend: {
						display: false,
					},
					tooltip: {
						enabled: false
					}
				}
			},
		},
		init() {
			Alpine.nextTick(() => {
				this.chartEl = document.getElementById(this.chartId);
			});

			Alpine.effect(() => {
				const value = Alpine.store('directions').value;
				if (value) {
					this.direction = value.find((d: IDirectionTask) => d.code == this.directionCode);
					this.task = this.direction?.tasks.find(t => t.code == this.taskCode) || null;

					if (chart) {
						const data = this.getData(this.direction as IDirectionTask, this.task as ITaskData);
						const currentDataArray: number[] = chart.data.datasets[0].data;
						currentDataArray.splice(0, currentDataArray.length);
						currentDataArray.push(...data.datasets[0].data);
						chart.update();
					}
				}
			});
		},
		toggle() {
			this.opened = !this.opened;

			if (this.opened && !chart) {
				this.config.data = this.getData(this.direction as IDirectionTask, this.task as ITaskData);
				chart = new Chart(this.chartEl, this.config);
			}
		},
		getData(direction: IDirectionTask, currentTask: ITaskData) {
			diffs = [];
			const data: number[] = ROLE_CODES.map(role => {
				const a = direction.by_roles[role].find(t => t.code == currentTask.code) as ITaskData;
				diffs.push(a?.diff);
				return a?.index || 0;
			});

			return {
				labels: ROLE_CODES.map(role => ROLE_LABELS[role]),
				datasets: [
					{
						label: 'parent',
						data: data,
						backgroundColor: DIRECTION_COLORS[direction.code]
					}
				]
			};
		}
	}
}