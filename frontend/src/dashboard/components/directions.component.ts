declare const Alpine: any;

import { DIRECTIONS, DIRECTION_LABELS, SCHOOL_INDEX_HIGH, SCHOOL_INDEX_MIDDLE } from '../constants';
import { IDirectionTask } from './directions.store';

export function rfDirectionsComponent() {
	return {
		directions: [] as IDirectionTask[],
		school_index_middle: SCHOOL_INDEX_MIDDLE,

		init() {
			Alpine.effect(() => {
				const value = Alpine.store('directions').value;
				if (value) {
					this.directions = value;

					const directionValues = this.directions.reduce((prev: Record<string, number | null>, d) => {
						prev[d.code] = d.index ?? '-' as any;
						return prev;
					}, {});

					Alpine.nextTick(() => {
						this.initRecommendations(directionValues);
						replaceData(directionValues);
					});
				}
			});
		},

		getDirectionsList(survey: any) {
			return DIRECTIONS.map(D => {
				const dValue = survey.stats.directions[D];
				return {
					code: D,
					index: Math.round(dValue.index || 0),
					name: DIRECTION_LABELS[D].join(' ')
				};
			});
		},

		initRecommendations(survey: Record<string, number | null>) {
			const commonRecContainer = $('.rf-recommendation .rf-text');
			const allBlocks = $('#rec349184091 .t668__col');
			allBlocks.hide();
			allBlocks.parent().css({ 'min-height': '1200px' });

			// if (responseCountStats.current_value < responseCountStats.low_segment) {
			// 	commonRecContainer.text(`Пока слишком мало данных. Попросите пройти опрос как можно большее количество участников сообщества.`);
			// 	return;
			// }

			let directionsSum = 0;
			for (let i = 0; i < DIRECTIONS.length; i++) {
				const D = DIRECTIONS[i];
				const elIndex = i * 3;
				const lowRec = $(allBlocks[elIndex]);
				const midRec = $(allBlocks[elIndex + 1]);
				const higRec = $(allBlocks[elIndex + 2]);

				const dValue = survey[D] || 0;
				directionsSum += dValue;
				const directionBlock = $(`#direction_${D}`);
				directionBlock.off('click');
				if (dValue < SCHOOL_INDEX_MIDDLE) {
					lowRec.show();
					directionBlock.on('click', () => lowRec.find('.t668__header').trigger('click'));
				}
				else if (dValue < SCHOOL_INDEX_HIGH) {
					midRec.show();
					directionBlock.on('click', () => midRec.find('.t668__header').trigger('click'));
				}
				else {
					higRec.show();
					directionBlock.on('click', () => higRec.find('.t668__header').trigger('click'));
				}
			}

			const directionsAvg = Math.round(directionsSum / 9);
			if (directionsAvg < SCHOOL_INDEX_MIDDLE) {
				commonRecContainer.text(`Вы на верном пути! Изучите рекомендации ниже, чтобы узнать в каких направлениях можно развивать школу дальше`);
			}
			else if (directionsAvg < SCHOOL_INDEX_HIGH) {
				commonRecContainer.text(`Отличный результат! Посмотрите рекомендации, чтобы узнать, в каких еще направлениях может развиваться сообщество`);
			}
			else {
				commonRecContainer.text(`У вас сильная школа и сообщество! Но нет предела совершенству — загляните в раздел рекомендаций, чтобы найти новые идеи`);
			}
		}
	}
}

function replaceData(data: Record<string, any>) {
	for (const key in data) {
		if (Object.hasOwnProperty.call(data, key)) {
			const value = data[key];
			$(`[data-replace-key=${key}]`).text(value);
		}
	}
}