import { ICommunityFullModel } from '../../../../models';
import { COMMUNITY_API_URL } from '../constants';
declare const Alpine: any;

const DAY_MS = 1000 * 60 * 60 * 24;


export type DailyActivity = {
	date: string;
	count: number;
}

export function rfActivityStore() {

	return {
		value: null as Record<string, DailyActivity[]> | null,
		init() {
			Alpine.effect(() => {
				const community: ICommunityFullModel = Alpine.store('community').value;
				if (community?.surveys) {
					this.fetchActivity(community.id).then(activity => this.value = this.fillAllDays(activity))
				}
			});
		},
		fetchActivity(comId: string) {
			return fetch(`${COMMUNITY_API_URL}/${comId}/surveys/activity`).then(r => r.json())
		},
		fillAllDays(activity: Record<string, any>) {

			for (const sid in activity) {
				if (Object.prototype.hasOwnProperty.call(activity, sid)) {
					const surveyActivity = activity[sid];

					const activityData: DailyActivity[] = [];

					const dates = Object.keys(surveyActivity).map(d => parseInt(d));

					if (dates.length) {
						let currentDate = dates[0] + DAY_MS;

						activityData.push({
							date: new Date(currentDate).toLocaleDateString(),
							count: surveyActivity[currentDate.toString()]
						});

						while (currentDate <= dates[dates.length - 1]) {
							currentDate += DAY_MS;
							if (!surveyActivity[`${currentDate}`]) {

								activityData.push({
									date: new Date(currentDate).toLocaleDateString(),
									count: 0
								})
							}
							else {
								activityData.push({
									date: new Date(currentDate).toLocaleDateString(),
									count: surveyActivity[currentDate.toString()]
								})
							}
						}
					}

					activity[sid] = activityData;
				}
			}

			return activity;
		}
	}
}

