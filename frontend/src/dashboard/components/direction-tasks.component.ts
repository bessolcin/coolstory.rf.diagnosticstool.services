import { IDirectionTask } from './directions.store';
declare const Alpine: any;

export function rfDirectionTasks() {
	return {
		directions: [] as IDirectionTask[],
		init() {
			Alpine.effect(() => {
				const value = Alpine.store('directions').value;
				
				if (value) {
					// this.directions = [];
					value.forEach((d: any) => {
						d.diff_text = this.getDiffText(d);
					});
					Alpine.nextTick(() => {
						this.directions = value;
					});
				}
			});
		},
		getDiffText(direction: IDirectionTask) {
			if (typeof (direction.diff) == 'number') {
				const surveyLabel = Alpine.store('survey_index_comparabale').is_last_survey ? 'последним' : 'прошлым';
				return `${direction.diff >= 0 ? '+' : '−'}${Math.abs(direction.diff)}% по сравнению с ${surveyLabel} опросом`;
			}
			return '';
		}
	};
}