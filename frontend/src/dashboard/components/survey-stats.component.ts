import { ICommunityFullModel } from '../../../../models';
import { COMMUNITY_API_URL } from '../constants';

declare const Alpine: any;

export function rfSurveyStats() {
	return {
		survey: null as (any | null),
		count: null as any,
		com_id: null,
		init: function () {
			Alpine.effect(() => {
				const community = Alpine.store('community').value;
				if (community?.surveys) {
					this.com_id = community.id;
					const index = Alpine.store('survey_index').index;
					const survey = community.surveys[index];
					this.survey = survey;
					this.count = this.getCountStats(community, survey);
				}
			});
		},
		get certificate_url() {
			return (this.com_id && this.survey?.id) ?
				`${COMMUNITY_API_URL}/${this.com_id}/surveys/${this.survey.id}/certificate` :
				'';
		},
		getCountStats: function (community: ICommunityFullModel, survey: any) {
			const actualSurveyResponsesPercent = Math.round(survey.stats.count / community.approx_community_size * 100);

			const lowSegmentWidth = Math.round(community.sample_count.total / community.approx_community_size * 100);
			const middleSegmentWidth = Math.max(70 - lowSegmentWidth, 0);
			const highSegmentWidth = 100 - lowSegmentWidth - middleSegmentWidth;
			const totalSample = community.sample_count.total + 1;

			function getPercent(value: number, total: number) {
				return Math.round(Math.min(1, value / total) * 100);
			}

			const countStats = {
				required_sample: totalSample,
				actual_sample: survey.stats.count,

				low_segment: lowSegmentWidth,
				middle_segment: middleSegmentWidth,
				high_segment: highSegmentWidth,
				current_value: actualSurveyResponsesPercent,

				total_sample: totalSample,
				community_size_70: Math.round(community.approx_community_size * 0.7),
				approx_community_size: community.approx_community_size,

				responses: [
					{
						key: 'total',
						name: 'Всего ответов',
						value: survey.stats.count,
						total: totalSample,
						get percent() {
							return getPercent(this.value, this.total);
						}
					},
					{
						key: 'students',
						name: 'Учеников',
						value: survey?.stats.by_roles.student.count,
						total: community.sample_count.student,
						get percent() {
							return getPercent(this.value, this.total);
						}
					},
					{
						key: 'total',
						name: 'Учителей',
						value: survey?.stats.by_roles.teacher.count,
						total: community.sample_count.teacher,
						get percent() {
							return getPercent(this.value, this.total);
						}
					},
					{
						key: 'total',
						name: 'Родителей',
						value: survey?.stats.by_roles.parent.count,
						total: community.sample_count.parent,
						get percent() {
							return getPercent(this.value, this.total);
						}
					},
					{
						key: 'total',
						name: 'Выпускников',
						value: survey?.stats.by_roles.graduate.count,
						total: community.sample_count.graduate,
						get percent() {
							return getPercent(this.value, this.total);
						}
					},
					{
						key: 'total',
						name: 'Партнёров',
						value: survey?.stats.by_roles.partner.count,
						total: 1,
						get percent() {
							return getPercent(this.value, this.total);
						}
					},
				]
			};


			return countStats;
		}
	}
}