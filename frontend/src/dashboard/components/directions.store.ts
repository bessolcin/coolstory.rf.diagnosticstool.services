import { ICommunityFullModel, ISurvey, ICommunityOverallStats, ICommunityStats } from '../../../../models';
import { DIRECTIONS, DIRECTION_LABELS, DIRECTION_LEVELS, ROLE_CODES, TASK_LABELS } from '../constants';
declare const Alpine: any;

export interface IDirectionTask {
	code: string;
	index: number | null;
	name: string;
	tasks: ITaskData[];
	by_roles: {
		[key: string]: ITaskData[]
	}
	diff?: number;
	level: null | 1 | 2 | 3;
}

export interface ITaskData {
	name: string;
	code: string;
	index: number | null;
	diff: number | null;
}

export function rfDirectionsStore() {

	return {
		value: [] as IDirectionTask[],
		comparable_value: null as IDirectionTask[] | null,
		init() {
			Alpine.effect(() => {
				const community: ICommunityFullModel = Alpine.store('community').value;
				if (community?.surveys) {
					const index = Alpine.store('survey_index').index;
					const comparableIndex = Alpine.store('survey_index_comparabale').index;

					const survey = community.surveys[index];
					if (!survey) return;
					
					const currentValue: IDirectionTask[] = this.initDirectionTasks(survey);

					if (typeof (comparableIndex) == 'number' && comparableIndex != index) {
						const comparableSurvey = community.surveys[comparableIndex];
						const comparableDirections = this.initDirectionTasks(comparableSurvey);
						this.comparable_value = comparableDirections;

						DIRECTIONS.forEach((D, i) => {
							currentValue[i].diff = (currentValue[i].index ?? 0) - (comparableDirections[i].index ?? 0);

							ROLE_CODES.forEach(role => {

								currentValue[i].by_roles[role].forEach((val: ITaskData, ti: number) => {
									const comparableVal = comparableDirections[i].by_roles[role][ti].index;

									if (typeof (comparableVal) == 'number' && typeof (val.index) == 'number') {
										val.diff = val.index - comparableVal;
									}
									else {
										val.diff = null;
									}
								})

							})

						});
					}
					else {
						this.comparable_value = null;
					}
					this.value = currentValue;
				}
			});
		},
		initDirectionTasks(survey: ISurvey): IDirectionTask[] {
			const stats = survey.stats as ICommunityOverallStats;

			return DIRECTIONS.map(D => (<IDirectionTask>{
				code: D,
				index: roundOrNull(stats.directions[D].index),
				level: getLevel(D, stats),
				name: DIRECTION_LABELS[D].join(' '),
				tasks: getTasksData(D, stats),
				by_roles: ROLE_CODES.reduce((by_roles: Record<string, ITaskData[]>, role) => {
					by_roles[role] = getTasksData(D, stats.by_roles[role]);
					return by_roles;
				}, {})
			}));
		}
	}
}

function getLevel(D: string, stats: ICommunityStats) {
	const index = stats.directions[D].index;
	if (typeof (index) == 'undefined' || index == null) {
		return null;
	}
	if (index < DIRECTION_LEVELS[D][0]) return 1;
	if (index < DIRECTION_LEVELS[D][1]) return 2;
	return 3;
}

function getTasksData(D: string, stats: ICommunityStats) {
	const tasksValues: ITaskData[] = [];
	for (let j = 1; j <= 3; j++) {
		const T = `${D}T${j}`;

		tasksValues.push({
			name: TASK_LABELS[T],
			code: T,
			index: roundOrNull(stats.tasks[T].index),
			diff: null
		});
	}
	return tasksValues;
}

function roundOrNull(value: any) {
	if (value === null || typeof (value) != 'number') {
		return null;
	}
	return Math.round(value);
}