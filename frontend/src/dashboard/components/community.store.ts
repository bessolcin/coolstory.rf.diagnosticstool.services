import { getOrgName, ICommunityFullModel, ICommunityStats, isDadataOrg, isManualOrg } from '../../../../models';
import { COMMUNITY_API_URL, DIRECTIONS } from '../constants';

const STATE_CACHE_LIFETIME = 8;

export function rfCommunityStore(withStats: boolean) {
	return {
		value: null,
		isLight: false,
		async init() {
			console.log('community init');
			const params = new URLSearchParams(window.location.search);
			const communitylId = params.get('c');
			replaceAllLinks(params);

			if (!communitylId) {
				this.naviagateToIndex();
				return;
			}

			this.isLight = params.get('light') === 'true';

			this.loadCommunityData(communitylId, withStats)
				.then((comData: unknown): void => {
					const community = <ICommunityFullModel>comData;

					if (!community?.id) {
						this.naviagateToIndex();
						return;
					}

					this.calculateDiff(community);
					this.fillCommunityName(community);
					this.fillCity(community);
					this.update(community);

					setTimeout(() => {
						replaceData(community);
					}, 100);
				});
		},
		update(val: any) {
			this.value = val;
		},
		naviagateToIndex() {
			if (window.location.pathname != '/diag') {
				window.location.href = '/diag';
			}
		},
		loadCommunityData(communitylId: string, withStats: boolean) {
			return new Promise((resolve, reject) => {
				const commState = loadState(communitylId);
				if (commState && commState.value?.surveys[0]) {
					resolve(commState.value);
				}
				else {
					getCommunityData(communitylId, withStats)
						.done((response: any) => {
							const commData: ICommunityFullModel = response.result;
							if (Array.isArray(commData.surveys)) {
								const last = commData.surveys.pop();
								commData.surveys = commData.surveys.length > 0 ? commData.surveys.filter(s => !!s.stats?.count) : commData.surveys;
								if (last) {
									commData.surveys.push(last);
								}
							}
							saveState(communitylId, {
								value: commData,
								timestamp: (new Date()).getTime()
							});
							resolve(response.result);
						});
					// .error(reject);
				}
			});
		},
		calculateDiff(comData: ICommunityFullModel) {
			if (comData.surveys) {
				const lastSurvey = comData.surveys[comData.surveys?.length - 1];
				for (let i = 0; i < comData.surveys.length - 1; i++) {
					const surveyStats = comData.surveys[i]?.stats as ICommunityStats;

					for (let j = 0; j < DIRECTIONS.length; j++) {
						const D = DIRECTIONS[j];

						if (typeof (surveyStats?.directions[D]?.index) == 'number' &&
							typeof (lastSurvey.stats?.directions[D].index) == 'number') {
							(<any>surveyStats.directions[D]).diff = <number>(surveyStats.directions[D].index) - <number>lastSurvey.stats?.directions[D].index;
						}
					}
					(<any>surveyStats).is_last = false;
				}
			}
		},
		fillCommunityName(community: ICommunityFullModel) {
			const orgName = getOrgName(community.organization);
			(<any>community).community_short_name =
				orgName.length > 50 ?
					orgName.substring(0, 50) + '…' :
					orgName;

			if (community && community.email) {
				const emailParts = community.email.split('@');
				const maskedEmail = `${emailParts[0].substring(0, 3)}***${emailParts[0].substring(emailParts[0].length - 3)}@${emailParts[1]}`;
				(<any>community).masked_email = maskedEmail;
			}
		},
		fillCity(community: ICommunityFullModel) {
			if (isManualOrg(community.organization)) {
				const city = community.organization.city ? community.organization.city : community.organization.settlement
				community.city = city;
				community.organization.city = city;
			}
			else if (isDadataOrg(community.organization)) {
				const address = community.organization.address.data;
				community.city = address.city ? `${address.city_type} ${address.city}` : `${address.settlement_type} ${address.settlement}`;
			}
		}
	}
}

function getCommunityData(schoolId: string, withStats: boolean) {
	return $.ajax({
		method: 'GET',
		url: `${COMMUNITY_API_URL}/${schoolId}?${withStats === true ? 'stats=true' : ''}`,
		// crossDomain: true,
		contentType: "application/json; charset=utf-8",
		error: (e: any) => {
			console.error(e);
		}
	}) as any;
}

type RfStateCache = { timestamp: number, value: any } | null;

function loadState(storageKey: string): RfStateCache {
	//Here should be the code to load the data from your database
	var stateStr: string | null = null;

	if (window.localStorage) {
		stateStr = window.localStorage.getItem(storageKey);
	}

	var surveyState: RfStateCache = null;
	if (stateStr) {
		try {
			surveyState = JSON.parse(stateStr);
		}
		catch (e) {
			console.error(`State under key ${storageKey} is corrupted: ${stateStr}`);
		}
	}

	const now = (new Date()).getTime();
	if (surveyState && getDiffInMinutes(now, surveyState.timestamp) <= STATE_CACHE_LIFETIME) {
		return surveyState;
	}
	return null;
}

function saveState(key: string, object: any) {
	if (window.localStorage) {
		if (object == null) {
			window.localStorage.removeItem(key);
			return;
		}
		window.localStorage.setItem(key, JSON.stringify(object));
	}
}

function getDiffInMinutes(d1: number, d2: number) {
	return Math.abs(Math.round(Math.abs(d1 - d2) / (1000 * 60)));
}

export function replaceAllLinks(params: any) {
	const replaceButtons = window['RFReplaceButtons' as any];
	if (replaceButtons) {
		for (let i = 0; i < replaceButtons.length; i++) {
			const id = <string><unknown>replaceButtons[i];
			appendQueryToLink(id, params.toString());
		}
	}
}

export function appendQueryToLink(elemId: string, query: string) {
	let el = $(`[data-elem-id=${elemId}]>a`);
	if (!el.length) {
		el = $(`#${elemId} a`);
	}
	if (el.length) {
		const elementHref = <string>el.attr('href');
		el.attr('href', elementHref + (elementHref.indexOf('?') > -1 ? '&' : '?') + query);
	}
}


function replaceData(data: Record<string, any>) {
	for (const key in data) {
		if (Object.hasOwnProperty.call(data, key)) {
			const value = data[key];
			$(`[data-replace-key=${key}]`).text(value);
		}
	}
}