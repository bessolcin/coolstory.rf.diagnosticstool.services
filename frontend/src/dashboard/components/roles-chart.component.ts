import { ICommunityFullModel, ICommunityOverallStats, ICommunityStats } from '../../../../models';
import { DIRECTIONS, DIRECTION_LABELS, ROLE_LABELS, ROLE_COLORS, CHART_TEXT_COLOR, DATA_LABELS_COLOR, fontSetting, ROLE_CODES } from '../constants';
import { isMobile } from '../utils';

declare const ChartDataLabels: any;
declare const Chart: any;
declare const Alpine: any;

let ROLES_CHART: any;

document.fonts.onloadingdone = () => {
	ROLES_CHART?.update();
};

export function rfRolesChart() {

	return {
		config: <any>{
			type: 'bar',
			responsive: true,
			plugins: [ChartDataLabels],
			options: {
				barThickness: 30,
				borderWidth: 3,
				borderColor: 'rgba(0,0,0,0)',
				aspectRatio: 0.16,
				maintainAspectRatio: false,
				indexAxis: 'y',
				scales: {
					y: {
						ticks: fontSetting
					},
					x: {
						max: 100,
						beginAtZero: true,
						ticks: fontSetting
					}
				},
				elements: {
					bar: {
						borderRadius: 8
					}
				},
				plugins: {
					// Change options for ALL labels of THIS CHART
					datalabels: {
						anchor: 'end',
						align: 'left',
						clamp: false,
						offset: -30,
						labels: {
							title: {
								font: {
									weight: 'bold',
								},
								color: DATA_LABELS_COLOR
							}
						},
						formatter: function (value: any, context: any) {
							return context.dataset.data[context.dataIndex] ? `${context.dataset.label}    ${context.dataset.data[context.dataIndex]}%` : ' *';
						}
					},
					legend: {
						display: true,
						position: isMobile() ? 'top' : 'right',
						align: 'start',
						labels: {
							// boxWidth: 30,
							// boxHeight: 30,
							borderRadius: 5,
							// pointStyleWidth: 23,
							usePointStyle: true,
							pointStyle: 'circle',

							padding: 15,
							font: fontSetting,
							color: CHART_TEXT_COLOR
						}
					},
					tooltip: {
						enabled: false
					}
				}
			},
		},
		chart: null,
		init() {

			Alpine.effect(() => {
				const community: ICommunityFullModel | null = Alpine.store('community').value;

				if (community?.surveys) {
					const index = Alpine.store('survey_index').index;

					const chartData = this.getChartData(community, index);

					if (!ROLES_CHART) {
						this.initRolesChart(chartData);
					}
					else {
						this.updateRolesChart(chartData);
					}
				}
			});
		},
		initRolesChart(chartData: any) {
			this.config.data = chartData;
			const chartContainer = document.getElementById('directions_by_roles_chart');
			if (chartContainer) {
				ROLES_CHART = new Chart(chartContainer, this.config);
			}
		},

		updateRolesChart(chartData: any) {
			ROLES_CHART?.data.datasets.forEach((d: any, i: number) => {
				d.data.splice(0, d.data.length);
				d.data.push(...chartData.datasets[i].data);
			});

			ROLES_CHART?.update();
		},
		getChartData(community: ICommunityFullModel, syrveyIndex: number) {
			const labels = DIRECTIONS.map(d => [...DIRECTION_LABELS[d]]);
			const data: any = {
				labels: labels,
				datasets: []
			};

			const survey = community.surveys ? community.surveys[syrveyIndex] : null;

			if (survey) {
				const surveyStats = (<ICommunityOverallStats>survey.stats);

				ROLE_CODES.forEach(role => {
					const roleStats: ICommunityStats = (<any>surveyStats.by_roles)[role];

					data.datasets.push({
						label: `${ROLE_LABELS[role]}`,
						data: DIRECTIONS.map(d => Math.round(roleStats.directions[d].index || 0)),
						...ROLE_COLORS[role]
					});
				});

				for (const role in surveyStats.by_roles) {
					if (Object.hasOwnProperty.call(surveyStats.by_roles, role)) {


					}
				}
			}
			return data;
		}
	};
}