declare const Alpine: any;

export function rfSurveyDropdown() {
	return {
		open: false,
		key: null as (string | null),
		items: null as (any[] | null),
		selected: null,
		selectedIndex: null as (number | null),
		init(key: string) {
			this.key = this.key || key;
			Alpine.effect(() => {
				const community = this.key ? Alpine.store(this.key).value : null;
				if (community?.surveys) {
					const index = Alpine.store('survey_index').index;
					this.items = community.surveys;
					this.selectedIndex = index;
					this.selected = this.items ? this.items[index] : null;
				}
			});
		},
		select(index: number, target: any) {
			this.selectedIndex = index;
			this.selected = this.items ? this.items[index] : null;
			Alpine.store('survey_index').update(index);
			this.close(target);
		},
		toggle() {
			if (this.open) {
				return this.close()
			}

			(<any>this).$refs.button.focus();

			this.open = true
		},
		close(focusAfter?: any) {
			if (!this.open) return;

			this.open = false;

			focusAfter && focusAfter.focus();
		}
	}
}