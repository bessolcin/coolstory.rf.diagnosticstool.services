import { GLOBAL_PATH } from '../js/path';
const exceptions = ['registration-success', 'registration-error', 'oferta'];

$(function () {
	function redirectToMain() {
		const u = new URL(window.location.href);
		const segs = u.pathname.split('/').filter(s => !!s);
		const path = segs.slice(0, segs.length - 1).join('/');
		var communityId = segs.slice(-1);
		if (exceptions.includes(communityId)) return;
		window.location.href = `/${path}/community?c=${communityId}`;
	}

	redirectToMain();
})