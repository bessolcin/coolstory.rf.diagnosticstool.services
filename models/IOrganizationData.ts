export type OrganizationDataType = 'dadata' | 'manual';

export interface IOrganizationData {
	data_type: 'dadata' | 'manual';
}

export interface IManualOrganizationData extends IOrganizationData {
	data_type: 'manual';
	country: string;
	region: string;
	city?: string;
	settlement?: string;
	name: string;
}

export interface IDadataOrganizationData extends IOrganizationData {
	data_type: 'dadata';
	inn: string;
	name: IDadataOrganizationName;
	address: IDadataAddress;
}

export interface IDadataOrganizationName {
	full_with_opf: string;
	short_with_opf: string;
	latin: string | null;
	full: string;
	short: string;
}

export interface IDadataAddress {
	unrestricted_value: string;
	value: string;
	data: IDadataAdressDetails;
}

export interface IDadataAdressDetails {
	area: string | null;
	area_type: string | null;
	block: string | null;
	block_type: string | null;
	city: string | null;
	city_type: string | null;
	city_district: string | null;
	city_district_type: string | null;
	country: string | null;
	federal_district: string | null;
	flat: string | null;
	flat_type: string | null;
	geo_lat: string | null;
	geo_lon: string | null;
	geoname_id: string | null;
	house: string | null;
	house_type: string | null;
	region: string | null;
	region_type: string | null;
	qc_geo: string | null;
	settlement: string | null;
	settlement_type: string | null;
	stead: string | null;
	stead_type: string | null;
	street: string | null;
	street_type: string | null;
	timezone: string | null;
}

export function isDadataOrg(orgData: IOrganizationData): orgData is IDadataOrganizationData {
	return orgData.data_type == 'dadata';
}

export function isManualOrg(orgData: IOrganizationData): orgData is IManualOrganizationData {
	return orgData.data_type == 'manual';
}

export function getOrgName(orgData: IDadataOrganizationData | IManualOrganizationData): string {
	return isDadataOrg(orgData) ? orgData.name.short_with_opf : orgData.name;
}