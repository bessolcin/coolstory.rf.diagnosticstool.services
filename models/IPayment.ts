export enum PyamentStatus {
	NEW = 'NEW',
	PAID = 'PAID'
}

export interface IPayment {
	id: string;
	community_id: string;
	status: PyamentStatus;
	created: number;
	created_str: string;
	payment_data?: any;
}