import { IManualOrganizationData, IDadataOrganizationData } from './IOrganizationData';
import { ICommunityOverallStats } from './stats';

export interface ICommunity {
	/**	
	 * @deprecated
	 */
	country?: string;
	/**	
	 * @deprecated
	 */
	region?: string;
	/**	
	 * @deprecated
	 */
	city?: string;
	/**	
	 * @deprecated
	 */
	settlement?: string;
	/**	
	 * @deprecated
	 */
	/**	
	 * @deprecated
	 */
	school?: string;

	email: string;
	name: string;

	organization: IManualOrganizationData | IDadataOrganizationData;

	students_count: number;
	teachers_count: number;
	graduate_estimate: number;
}

export interface ICommunityViewModel extends ICommunity {
}

export interface ICommunityDB extends ICommunity {
	id?: string,
	_timestamp: number;
	_is_rsa: boolean;
	approx_community_size: number;
	sample_count: ICommunitySample;
	surveys: Record<string, ISurvey>;
}

export interface ICommunityFullModel extends ICommunity {
	id: string;
	_timestamp: number;
	_is_rsa: boolean;
	approx_community_size: number;
	sample_count: ICommunitySample;
	stats: ICommunityOverallStats | null;
	surveys: ISurvey[] | null;
}

export interface ICommunitySample {
	student: number;
	teacher: number;
	parent: number;
	graduate: number;
	total: number;
}

export interface ISurvey {
	id?: string;
	start_date: number;
	start_date_str?: string;
	datetime_offset: number;
	stats?: ICommunityOverallStats;
}