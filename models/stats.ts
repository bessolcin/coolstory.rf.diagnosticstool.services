export type RolesT = 'student' | 'teacher' | 'parent' | 'graduate' | 'partner';


export interface ICommunityStats {
	count: number;
	directions: IStatsTable;
	tasks: IStatsTable;
}

export interface ICommunityOverallStats extends ICommunityStats {
	by_roles: IRoleStats;
}

export interface IRoleStats {
	[key: string]: ICommunityStats;
	student: ICommunityStats,
	teacher: ICommunityStats,
	parent: ICommunityStats,
	graduate: ICommunityStats,
	partner: ICommunityStats,
}

export interface IStatsTable {
	[direction: string]: IStatsItem;
}

export interface IStatsItem {
	index?: number;
	sum: number;
	L1: number;
	L2: number;
	L3: number;
}