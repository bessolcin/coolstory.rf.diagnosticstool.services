export interface ICreateCommunityResponse {
	message?: string;
	result: 'ok' | 'error';
	id: string;
	error?: string;
}