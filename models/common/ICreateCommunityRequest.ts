import { IDadataOrganizationData, IManualOrganizationData } from '../IOrganizationData';

export interface ICreateCommunityRequest {
	organization: IDadataOrganizationData | IManualOrganizationData;

	teachers_count: number;
	students_count: number;
	graduate_estimate: number;

	name: string;
	email: string;

	payment_id?: string;
}